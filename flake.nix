{
  description = "persional neovim config in flake";
  inputs = {
    commitlint-config.url = "git+https://codeberg.org/samuelsung/commitlint-config";
    commitlint-config.inputs.nixpkgs.follows = "nixpkgs";
    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";
    misc.url = "git+https://codeberg.org/samuelsung/misc";
    neorg-overlay.url = "github:nvim-neorg/nixpkgs-neorg-overlay";
    nixvim.url = "github:pta2002/nixvim";
    nixvim.inputs.nixpkgs.follows = "nixpkgs";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        ./checks
        ./devenv
        ./wrappers
        ./packages
        ./srcs.nix
      ];
      systems = [
        "aarch64-linux"
        "x86_64-linux"
      ];
    };
}
