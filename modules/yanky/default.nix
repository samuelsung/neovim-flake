{ config, lib, ... }:

let
  inherit (lib) mkForce mkIf;
  cfg = config.plugins.yanky;
in
mkIf cfg.enable {
  plugins.yanky.extraOptions = {
    highlight = {
      on_yank = true;
      on_put = true;
      timer = 500;
    };

    preserve_cursor_position.enabled = true;
  };

  modules.visuals.highlightOnYank.enable = mkForce false;

  keymaps = [
    {
      mode = "n";
      key = "<leader>p";
      action = ''
        function()
          require("telescope").extensions.yank_history.yank_history({ })
        end
      '';
      lua = true;
      options = {
        silent = true;
        desc = "Open Yank History";
      };
    }

    {
      mode = [ "n" "x" ];
      key = "y";
      action = "<Plug>(YankyYank)";
      options = {
        desc = "Yank text";
      };
    }

    {
      mode = [ "n" "x" ];
      key = "p";
      action = "<Plug>(YankyPutAfter)";
      options = {
        desc = "Put yanked text after cursor";
      };
    }

    {
      mode = [ "n" "x" ];
      key = "P";
      action = "<Plug>(YankyPutBefore)";
      options = {
        desc = "Put yanked text before cursor";
      };
    }

    {
      mode = [ "n" "x" ];
      key = "gp";
      action = "<Plug>(YankyGPutAfter)";
      options = {
        desc = "Put yanked text after selection";
      };
    }

    {
      mode = [ "n" "x" ];
      key = "gP";
      action = "<Plug>(YankyGPutBefore)";
      options = {
        desc = "Put yanked text before selection";
      };
    }

    {
      mode = "n";
      key = "[y";
      action = "<Plug>(YankyCycleForward)";
      options = {
        desc = "Cycle forward through yank history";
      };
    }

    {
      mode = "n";
      key = "]y";
      action = "<Plug>(YankyCycleBackward)";
      options = {
        desc = "Cycle backward through yank history";
      };
    }

    {
      mode = "n";
      key = "]p";
      action = "<Plug>(YankyPutIndentAfterLinewise)";
      options = {
        desc = "Put indented after cursor (linewise)";
      };
    }

    {
      mode = "n";
      key = "[p";
      action = "<Plug>(YankyPutIndentBeforeLinewise)";
      options = {
        desc = "Put indented before cursor (linewise)";
      };
    }

    {
      mode = "n";
      key = "]P";
      action = "<Plug>(YankyPutIndentAfterLinewise)";
      options = {
        desc = "Put indented after cursor (linewise)";
      };
    }

    {
      mode = "n";
      key = "[P";
      action = "<Plug>(YankyPutIndentBeforeLinewise)";
      options = {
        desc = "Put indented before cursor (linewise)";
      };
    }

    {
      mode = "n";
      key = ">p";
      action = "<Plug>(YankyPutIndentAfterShiftRight)";
      options = {
        desc = "Put and indent right";
      };
    }

    {
      mode = "n";
      key = "<p";
      action = "<Plug>(YankyPutIndentAfterShiftLeft)";
      options = {
        desc = "Put and indent left";
      };
    }

    {
      mode = "n";
      key = ">P";
      action = "<Plug>(YankyPutIndentBeforeShiftRight)";
      options = {
        desc = "Put before and indent right";
      };
    }

    {
      mode = "n";
      key = "<P";
      action = "<Plug>(YankyPutIndentBeforeShiftLeft)";
      options = {
        desc = "Put before and indent left";
      };
    }

    {
      mode = "n";
      key = "=p";
      action = "<Plug>(YankyPutAfterFilter)";
      options = {
        desc = "Put after applying a filter";
      };
    }

    {
      mode = "n";
      key = "=P";
      action = "<Plug>(YankyPutBeforeFilter)";
      options = {
        desc = "Put before applying a filter";
      };
    }
  ];
}
