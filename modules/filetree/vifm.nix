{ pkgs, config, lib, ... }:

with lib;
with builtins;

let
  inherit (config.vim) scope;
  cfg = config.vim.file.vifm;
in
{
  options.vim.file.vifm = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable vifm";
    };
  };

  config = mkIf (cfg.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [ vifm-vim ];

    vim.nnoremap = {
      "<C-f>f" = ":Vifm<CR>";
      "<C-f>v" = ":VsplitVifm<CR>";
      "<C-f>s" = ":SplitVifm<CR>";
      "<C-f>t" = ":TabVifm<CR>";
    };
  };
}
