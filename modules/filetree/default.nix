{ pkgs, lib, config, ... }:
{
  imports = [
    ./nvimtreelua.nix
    ./vifm.nix
  ];
}
