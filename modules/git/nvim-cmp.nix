{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
{
  config = mkIf (config.modules.git.enable && !config.vscode.enable) {
    plugins.cmp-git.enable = true;
    plugins.cmp-tmux.enable = true;
    plugins.cmp-buffer.enable = true;
    plugins.cmp-rg.enable = true;

    extraConfigLua = ''
      require("cmp_git").setup()

      require('cmp').setup.filetype('gitcommit', {
        sources = require('cmp').config.sources({
          { name = 'buffer' },
          { name = 'tmux' },
          { name = 'rg' },
          { name = 'git' },
        })
      })
    '';
  };
}
