{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  plugins.gitsigns.enable = mkDefault (config.modules.git.enable && !config.vscode.enable);

  plugins.gitsigns.settings.signs = {
    add.text = "│";
    change.text = "│";
    delete.text = "_";
    topdelete.text = "‾";
    changedelete.text = "│";
  };

  plugins.gitsigns.settings.numhl = true;
  plugins.gitsigns.settings.linehl = false;
  plugins.gitsigns.settings.current_line_blame = true;
  plugins.gitsigns.settings.watch_gitdir.interval = 500;
}
