{ pkgs, config, lib, ... }:

with lib;
with builtins;

let
  inherit (config.vim) scope;
  cfg = config.vim.git.gitsigns;
in
{
  options.vim.git.gitsigns = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable gitsigns";
    };
  };

  config = mkIf (cfg.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [
      gitsigns-nvim
    ];

    vim.nnoremap = {
      "<expr> [c" = "&diff ? '[c' : '<cmd>lua require(\"gitsigns\").prev_hunk()<CR>'";
      "<expr> ]c" = "&diff ? ']c' : '<cmd>lua require(\"gitsigns\").next_hunk()<CR>'";
      "ga" = "<cmd>lua require(\"gitsigns\").stage_hunk()<CR>";
      "gr" = "<cmd>lua require(\"gitsigns\").reset_hunk()<CR>";
      "gb" = "<cmd>lua require(\"gitsigns\").blame_line()<CR>";
      "gp" = "<cmd>lua require(\"gitsigns\").preview_hunk()<CR>";
    } // (if (config.vim.keyboardLayout.layout == "colemak") then {
      "gl" = "<cmd>lua require(\"gitsigns\").undo_stage_hunk()<CR>";
    } else {
      "gu" = "<cmd>lua require(\"gitsigns\").undo_stage_hunk()<CR>";
    });

    vim.luaConfigRC = ''
      require('gitsigns').setup({
        signs = {
          add          = {hl = 'GitSignsAdd'   , text = '|', numhl='GitSignsAddNr'   , linehl='GitSignsAddLn'},
          change       = {hl = 'GitSignsChange', text = '|', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
          delete       = {hl = 'GitSignsDelete', text = '_', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
          topdelete    = {hl = 'GitSignsDelete', text = '‾', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
          changedelete = {hl = 'GitSignsChange', text = '|', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
        },
        numhl = true,
        linehl = false,

        keymaps = {},

        watch_index = {
          interval = 1000,
        },

        current_line_blame = true,
      })

      -- link GitSignsCurrentLineBlame to Comment, which is nord_3_bright in nord
      vim.cmd('highlight link GitSignsCurrentLineBlame Comment');
    '';
  };
}
