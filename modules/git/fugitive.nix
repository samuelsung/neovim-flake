{ config, lib, ... }:

let
  inherit (lib) mkDefault optionals;
  cfg = config.plugins.fugitive;
in
{
  plugins.fugitive.enable = mkDefault (config.modules.git.enable && !config.vscode.enable);
  keymaps = optionals cfg.enable [
    {
      mode = "n";
      key = "<C-G>c";
      action = ":Git commit";
    }

    {
      mode = "n";
      key = "<C-G> ";
      action = ":Git ";
    }
    {
      mode = "n";
      key = "<C-G>rb";
      action = ":Git rebase";
    }

    {
      mode = "n";
      key = "<C-G>ri";
      action = ":Git rebase -i";
    }
  ];
}
