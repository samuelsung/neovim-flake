{ config, lib, helpers, ... }:

let
  inherit (helpers) mkRaw;
  inherit (lib) mkEnableOption optionalString optionals mkIf;
  inherit (config.modules.remaps) keys;
in
{
  options.modules.git.enable = mkEnableOption "git support";

  imports = [
    ./fugitive.nix
    ./gitsigns.nix
    ./nvim-cmp.nix
  ];

  config.keymaps = optionals config.modules.git.enable [
    {
      mode = "n";
      key = "[c";
      action = mkRaw ''
        function()
          if vim.wo.diff then return '[c' end

          if vim.g.vscode then
              require('vscode').action('workbench.action.editor.previousChange')
              require('vscode').action('workbench.action.compareEditor.nextChange')
              return '<Ignore>'
          end

          ${optionalString config.plugins.gitsigns.enable ''
            vim.schedule((require("gitsigns")).prev_hunk)
          ''}
          return '<Ignore>'
        end
      '';
      options = {
        expr = true;
        silent = true;
        desc = "Previous git hunk";
      };
    }

    {
      mode = "n";
      key = "]c";
      action = mkRaw ''
        function()
          if vim.wo.diff then return ']c' end

          if vim.g.vscode then
              require('vscode').action('workbench.action.editor.nextChange')
              require('vscode').action('workbench.action.compareEditor.nextChange')
              return '<Ignore>'
          end

          ${optionalString config.plugins.gitsigns.enable ''
            vim.schedule(require('gitsigns').next_hunk)
          ''}

          return '<Ignore>'
        end
      '';
      options = {
        expr = true;
        silent = true;
        desc = "Next git hunk";
      };
    }

    (mkIf (config.vscode.enable || config.plugins.gitsigns.enable) {
      mode = "n";
      key = "<leader>gah";
      action = mkRaw ''
        function()
          if vim.g.vscode then
              require('vscode').action('git.stageSelectedRanges')
              return '<Ignore>'
          end

          ${if config.plugins.gitsigns.enable
            then "return ':Gitsigns stage_hunk<CR>'"
            else "return '<Ignore>'"}
        end
      '';
      options = {
        expr = true;
        silent = true;
        desc = "Git stage a hunk";
      };
    })

    (mkIf (config.vscode.enable || config.plugins.gitsigns.enable) {
      mode = "n";
      key = "<leader>grh";

      action = mkRaw ''
        function()
          if vim.g.vscode then
              require('vscode').action('git.revertSelectedRanges')
              return '<Ignore>'
          end

          ${if config.plugins.gitsigns.enable
            then "return ':Gitsigns reset_hunk<CR>'"
            else "return '<Ignore>'"}
        end
      '';
      options = {
        expr = true;
        silent = true;
        desc = "Git reset a hunk";
      };
    })

    (mkIf config.plugins.gitsigns.enable {
      mode = "n";
      key = "<leader>gb";
      action = ":Gitsigns blame_line<CR>";
      options = {
        silent = true;
        desc = "Git blame the current line";
      };
    })

    (mkIf config.plugins.gitsigns.enable {
      mode = "n";
      key = "<leader>gph";
      action = ":Gitsigns preview_hunk<CR>";
      options = {
        silent = true;
        desc = "Git preview a hunk";
      };
    })

    (mkIf (config.vscode.enable || config.plugins.gitsigns.enable) {
      mode = "n";
      key = "<leader>g${keys.undo}h";

      action = mkRaw ''
        function()
          if vim.g.vscode then
              require('vscode').action('git.unstageSelectedRanges')
              return '<Ignore>'
          end

          ${if config.plugins.gitsigns.enable
            then "return ':Gitsigns undo_stage_hunk<CR>'"
            else "return '<Ignore>'"}
        end
      '';
      options = {
        silent = true;
        desc = "Git undo the previous stage";
      };
    })

    (mkIf config.plugins.gitsigns.enable {
      mode = "n";
      key = "<leader>gd";
      action = mkRaw ''
        function()
          local gs = require("gitsigns")
          local show_deleted = gs.toggle_deleted()
          gs.toggle_word_diff(show_deleted)
        end
      '';
      options = {
        silent = true;
        desc = "Git show diff";
      };
    })
  ];
}
