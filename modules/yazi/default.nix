{ config, lib, helpers, ... }:

let
  inherit (lib) mkAfter mkIf;
  inherit (helpers) mkRaw;
  cfg = config.plugins.yazi;
in
mkIf cfg.enable {
  globals.loaded_netrwPlugin = 1;

  plugins.yazi.settings.open_for_directories = true;
  plugins.yazi.settings.enable_mouse_support = true;
  keymaps = mkAfter [
    {
      mode = "n";
      key = "<leader>ff";
      action = mkRaw ''
        function()
          require('yazi').yazi({ })
        end
      '';
      options = {
        silent = true;
        desc = "yazi";
      };
    }

    {
      mode = "n";
      key = "<leader>fv";
      action = mkRaw ''
        function()
          require('yazi').yazi({ })
        end
      '';
      options = {
        silent = true;
        desc = "yazi (same as <leader>ff)";
      };
    }

    {
      mode = "n";
      key = "<leader>fs";
      action = mkRaw ''
        function()
          require('yazi').yazi({ })
        end
      '';
      options = {
        silent = true;
        desc = "yazi (same as <leader>ff)";
      };
    }

    {
      mode = "n";
      key = "<leader>ft";
      action = mkRaw ''
        function()
          require('yazi').yazi({ })
        end
      '';
      options = {
        silent = true;
        desc = "yazi (same as <leader>ff)";
      };
    }
  ];
}
