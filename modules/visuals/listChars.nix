{ config, lib, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge mkOption types;
  cfg = config.listChars;
in
{
  options.listChars = {
    enable = mkOption {
      type = types.bool;
      default = config.modules.visuals.enable;
    };

    tab = mkOption {
      type = with types; nullOr str;
      default = "> ";
      description = ''
        tab:xy[z]  Two or three characters to be used to show a tab.
          The third character is optional.

        tab:xy  The 'x' is always used, then 'y' as many times as will
          fit. Thus "tab:>-" displays:  
            >
            >-
            >--
            etc.

        tab:xyz  The 'z' is always used, then 'x' is prepended, and
          then 'y' is used as many times as will fit.  Thus
          "tab:<->" displays:  
            >
            <>
            <->
            <-->
            etc.
        When "tab:" is omitted, a tab is shown as ^I.
      '';
    };

    space = mkOption {
      type = with types; nullOr str;
      default = null;
      description = ''
        space:c  Character to show for a space.  When omitted, spaces
        are left blank.
      '';
    };

    multispace = mkOption {
      type = with types; nullOr str;
      default = null;
      description = ''
        multispace:c...
          One or more characters to use cyclically to show for
          multiple consecutive spaces.  Overrides the "space"
          setting, except for single spaces.  When omitted, the
          "space" setting is used.  For example,
          `:set listchars=multispace:---+` shows ten consecutive
          spaces as:  
            ---+---+--
  
      '';
    };

    eol = mkOption {
      type = with types; nullOr str;
      default = null;
      description = ''
        Character to show at the end of each line. When
        omitted, there is no extra character at the end of the
        line.
      '';
    };

    lead = mkOption {
      type = with types; nullOr str;
      default = null;
      description = ''
        lead:c  Character to show for leading spaces.  When omitted,
          leading spaces are blank.  Overrides the "space" and
          "multispace" settings for leading spaces.  You can
          combine it with "tab:", for example:  
            :set listchars+=tab:>-,lead:.
      '';
    };

    leadmultispace = mkOption {
      type = with types; nullOr str;
      default = null;
      description = ''
        leadmultispace:c...
          Like the |lcs-multispace| value, but for leading
          spaces only.  Also overrides |lcs-lead| for leading
          multiple spaces.
          `:set listchars=leadmultispace:---+` shows ten
          consecutive leading spaces as:  
            ---+---+--XXX

          Where "XXX" denotes the first non-blank characters in
          the line.
      '';
    };

    trail = mkOption {
      type = with types; nullOr str;
      default = "-";
      description = ''
        trail:c  Character to show for trailing spaces.  When omitted,
          trailing spaces are blank.  Overrides the "space" and
          "multispace" settings for trailing spaces.
      '';
    };

    extends = mkOption {
      type = with types; nullOr str;
      default = null;
      description = ''
        extends:c  Character to show in the last column, when 'wrap' is
          off and the line continues beyond the right of the
          screen.
      '';
    };

    precedes = mkOption {
      type = with types; nullOr str;
      default = null;
      description = ''
        precedes:c  Character to show in the first visible column of the
          physical line, when there is text preceding the
          character visible in the first column.
      '';
    };

    conceal = mkOption {
      type = with types; nullOr str;
      default = null;
      description = ''
        conceal:c  Character to show in place of concealed text, when
          'conceallevel' is set to 1.  A space when omitted.
      '';
    };

    nbsp = mkOption {
      type = with types; nullOr str;
      default = "+";
      description = ''
        nbsp:c  Character to show for a non-breakable space character
          (0xA0 (160 decimal) and U+202F).  Left blank when
          omitted.
      '';
    };
  };

  config = mkMerge [
    {
      listChars.enable = mkDefault config.modules.visuals.enable;
      listChars.eol = "↴";
      listChars.trail = "-";
    }

    (mkIf cfg.enable {
      opts.list = true;

      opts.listchars = {
        inherit (cfg)
          tab
          space
          multispace
          eol
          lead
          leadmultispace
          trail
          extends
          precedes
          conceal
          nbsp;
      };
    })
  ];
}
