{ config, lib, ... }:

let
  # inherit (helpers) mkRaw;
  inherit (lib) mkIf mkMerge;
  cfg = config.colorschemes.nord;
in
mkMerge [
  {
    colorschemes.nord.enable = false;
    colorschemes.nord.settings.disable_background = true;
  }

  (mkIf cfg.enable {
    plugins.lualine.settings.options.theme = "nord";
  })

  (mkIf (cfg.enable && config.plugins.noice.enable) {
    # highlight.NoiceLspProgressTitle.fg = mkRaw "require('nord.named_colors').darkest_white";

    # highlight.NoiceMini.bg = mkRaw "require('nord.named_colors').dark_gray";
  })
]
