{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  plugins.deadcolumn-nvim = {
    enable = mkDefault config.modules.visuals.enable;
    extraOptions = {
      blending.hlgroup = [ "ColorColumn" "background" ];
      warning.alpha = 1;
      warning.hlgroup = [ "Error" "foreground" ];
    };
  };
}
