{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  plugins.indent-blankline.enable = mkDefault config.modules.visuals.enable;
  # SEE: :help ibl.config.indent.tab_char
  plugins.indent-blankline.settings.indent.char = "│";
  plugins.indent-blankline.settings.scope.show_start = true;
  plugins.indent-blankline.settings.scope.enabled = true;
}
