{ config, lib, ... }:

let
  inherit (lib) mkIf;
  cfg = config.plugins.noice;
in
{
  config.plugins.noice = {
    enable = false;
    settings.lsp = {
      # override markdown rendering so that **cmp** and other plugins use **Treesitter**
      override = {
        "vim.lsp.util.convert_input_to_markdown_lines" = true;
        "vim.lsp.util.stylize_markdown" = true;
        "cmp.entry.get_documentation" = true;
      };

      signature.enabled = mkIf config.plugins.lsp-signature.enable false;
    };

    # you can enable a preset for easier configuration
    settings.presets = {
      command_palette = true; # position the cmdline and popupmenu together
      # long_message_to_split = true; # long messages will be sent to a split
      # inc_rename = false; # enables an input dialog for inc-rename.nvim
      lsp_doc_border = true; # add a border to hover docs and signature help
    };

    settings.cmdline.enabled = true;
    settings.messages.enabled = true;

    settings.views.mini.win_options.winblend = 0;
  };

  config.opts.cmdheight = mkIf (cfg.enable && cfg.cmdline.enabled) 0;

  config.plugins.notify.enable = mkIf cfg.enable true;
}
