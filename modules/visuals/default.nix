{ config, helpers, lib, ... }:

let
  inherit (lib) mkEnableOption mkOption mkIf types;
  inherit (helpers) mkRaw;
  cfg = config.modules.visuals;
in
{
  options.modules.visuals.enable = mkEnableOption "visuals related options";
  options.modules.visuals.highlightOnYank.enable = mkEnableOption "highlight on yank";
  options.modules.visuals.highlightOnYank.timeout = mkOption {
    type = types.int;
    default = 500;
    description = "highlight on yank timeout";
  };

  config = mkIf cfg.enable {
    opts = {
      relativenumber = true;
      number = true;
      termguicolors = true;
      syntax = "on";
      laststatus = 3; # only last window have statusline
      winminwidth = 5; # Minimum window width
      pumheight = 10; # Maximum number of entries in a popup
      signcolumn = "yes"; # always show the sign column
    };

    modules.visuals.highlightOnYank.enable = true;

    autoCmd = [
      (mkIf cfg.highlightOnYank.enable {
        event = [ "TextYankPost" ];
        pattern = "*";
        callback = mkRaw ''
          function()
            vim.highlight.on_yank({
              higroup = 'IncSearch',
              timeout = ${toString cfg.highlightOnYank.timeout},
            })
          end
        '';
      })
    ];
  };

  imports = [
    ./catppuccin.nix
    ./deadcolumn-nvim
    ./indent-blankline.nix
    ./listChars.nix
    ./lualine.nix
    ./noice.nix
    ./nord.nix
    ./nvim-cursorline.nix
    ./nvim-notify.nix
    ./nvim-web-devicons.nix
  ];
}
