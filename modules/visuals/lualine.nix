{ config, lib, ... }:

let
  inherit (lib) mkDefault mkIf;
  cfg = config.plugins.lualine;
in
{
  config.plugins.lualine = {
    enable = mkDefault config.modules.visuals.enable;

    settings.options.icons_enabled = true;

    settings.sections.lualine_b = [
      "branch"
      "diff"
    ];

    settings.sections.lualine_c = [{
      __unkeyed-1 = "filename";
      file_status = true;
      newfile_status = false;
      path = 1;
      shorting_target = 40;
      symbols = {
        modified = "[+]";
        readonly = "[-]";
        unnamed = "[No Name]"; # Text to show for unnamed buffers.
        newfile = "[New]"; # Text to show for newly created file before first write
      };
    }];

    settings.extensions = [
      (mkIf config.plugins.nvim-tree.enable "nvim-tree")
    ];
  };

  config.opts.showmode = mkIf cfg.enable false; # do not show mode like --NORMAL--
}
