{ config, lib, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge;
  cfg = config.colorschemes.catppuccin;
in
mkMerge [
  {
    colorschemes.catppuccin = {
      enable = mkDefault config.modules.visuals.enable;
      settings.flavour = "mocha";
      settings.term_colors = true;
      settings.transparent_background = true;
    };
  }

  (mkIf cfg.enable {
    plugins.lualine.settings.options.theme = "catppuccin";
  })
]
