{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  plugins.cursorline = {
    enable = mkDefault config.modules.visuals.enable;

    settings.cursorline.enable = false;
    settings.cursorword = {
      enable = true;
      minLength = 3;
      hl.underline = true;
    };
  };
}
