{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  plugins.web-devicons = {
    enable = mkDefault config.modules.visuals.enable;
  };
}
