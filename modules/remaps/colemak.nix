{ config, lib, helpers, ... }:

let
  inherit (helpers) mkRaw;
  inherit (lib) mkIf;
  cfg = config.modules.remaps;
in
mkIf (cfg.layout == "colemak") {
  modules.remaps.keys.undo = "l";

  keymaps = [
    # motion
    {
      mode = [ "v" "n" ];
      key = "n";
      action = "j";
      options = {
        silent = true;
        remap = false;
      };
    }

    {
      mode = [ "v" "n" ];
      key = "N";
      action = "J";
      options = {
        silent = true;
        remap = false;
      };
    }

    {
      mode = [ "v" "n" ];
      key = "e";
      action = "k";
      options = {
        silent = true;
        remap = false;
      };
    }

    {
      mode = [ "v" "n" ];
      key = "E";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('editor.action.showHover')
              return '<Ignore>'
            else
              return 'K'
            end
          end
        '';
      options = {
        expr = true;
        silent = true;
        remap = false;
      };
    }

    {
      mode = [ "v" "n" ];
      key = "i";
      action = "l";
      options = {
        silent = true;
        remap = false;
      };
    }

    {
      mode = [ "v" "n" ];
      key = "I";
      action = "L";
      options = {
        silent = true;
        remap = false;
      };
    }

    # insert
    {
      mode = [ "v" "n" ];
      key = "u";
      action = "i";
      options = {
        silent = true;
        remap = false;
      };
    }

    {
      mode = [ "v" "n" ];
      key = "U";
      action = "I";
      options = {
        silent = true;
        remap = false;
      };
    }

    # undo
    {
      mode = "n";
      key = "L";
      action = "U";
      options = {
        silent = true;
        remap = false;
      };
    }

    # next
    {
      mode = "n";
      key = "k";
      action = "n";
      options = {
        silent = true;
        remap = false;
      };
    }

    {
      mode = "n";
      key = "K";
      action = "N";
      options = {
        silent = true;
        remap = false;
      };
    }

    {
      mode = "n";
      key = "<c-w>h";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.navigateLeft')
            else
              ${if config.plugins.tmux-navigator.enable
              then "return ':TmuxNavigateLeft<CR>'"
              else "return '<c-w>h'"}
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Go to the left window";
      };
    }

    {
      mode = "n";
      key = "<c-w>n";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.navigateDown')
            else
              ${if config.plugins.tmux-navigator.enable
              then "return ':TmuxNavigateDown<CR>'"
              else "return '<c-w>j'"}
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Go to the down window";
      };
    }

    {
      mode = "n";
      key = "<c-w>e";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.navigateUp')
            else
              ${if config.plugins.tmux-navigator.enable
              then "return ':TmuxNavigateUp<CR>'"
              else "return '<c-w>k'"}
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Go to the up window";
      };
    }

    {
      mode = "n";
      key = "<c-w>i";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.navigateRight')
            else
              ${if config.plugins.tmux-navigator.enable
              then "return ':TmuxNavigateRight<CR>'"
              else "return '<c-w>l'"}
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Go to the right window";
      };
    }

    {
      mode = "n";
      key = "<c-w>N";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.moveActiveEditorGroupDown')
            else
              return '<c-w>J'
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Move window to the bottom";
      };
    }

    {
      mode = "n";
      key = "<c-w>E";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.moveActiveEditorGroupUp')
            else
              return '<c-w>K'
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Move window to the top";
      };
    }

    {
      mode = "n";
      key = "<c-w>I";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.moveActiveEditorGroupRight')
            else
              return '<c-w>L'
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Move window to the right";
      };
    }

    {
      mode = "n";
      key = "<c-w>H";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.moveActiveEditorGroupLeft')
            else
              return '<c-w>H'
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Move window to the left";
      };
    }
  ];
}
