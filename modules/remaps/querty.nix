{ config, lib, helpers, ... }:

let
  inherit (helpers) mkRaw;
  inherit (lib) mkIf;
  cfg = config.modules.remaps;
in
mkIf (cfg.layout == "qwerty") {
  keymaps = [
    {
      mode = "n";
      key = "<c-w>h";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.navigateLeft')
            else
              ${if config.plugins.tmux-navigator.enable
              then "vim.cmd.TmuxNavigateLeft()"
              else "return '<c-w>h'"}
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Go to the left window";
      };
    }

    {
      mode = "n";
      key = "<c-w>j";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.navigateDown')
            else
              ${if config.plugins.tmux-navigator.enable
              then "vim.cmd.TmuxNavigateDown()"
              else "return '<c-w>j'"}
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Go to the down window";
      };
    }

    {
      mode = "n";
      key = "<c-w>k";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.navigateUp')
            else
              ${if config.plugins.tmux-navigator.enable
              then "vim.cmd.TmuxNavigateUp()"
              else "return '<c-w>k'"}
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Go to the up window";
      };
    }

    {
      mode = "n";
      key = "<c-w>l";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.navigateRight')
            else
              ${if config.plugins.tmux-navigator.enable
              then "vim.cmd.TmuxNavigateRight()"
              else "return '<c-w>l'"}
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Go to the right window";
      };
    }

    {
      mode = "n";
      key = "<c-w>J";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.moveActiveEditorGroupDown')
            else
              return '<c-w>J'
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Move window to the bottom";
      };
    }

    {
      mode = "n";
      key = "<c-w>K";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.moveActiveEditorGroupUp')
            else
              return '<c-w>K'
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Move window to the top";
      };
    }

    {
      mode = "n";
      key = "<c-w>L";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.moveActiveEditorGroupRight')
            else
              return '<c-w>L'
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Move window to the right";
      };
    }

    {
      mode = "n";
      key = "<c-w>H";
      action =
        mkRaw ''
          function()
            if vim.g.vscode then
              require('vscode').action('workbench.action.moveActiveEditorGroupLeft')
            else
              return '<c-w>H'
            end
          end
        '';
      options = {
        silent = true;
        noremap = true;
        expr = true;
        desc = "Move window to the left";
      };
    }
  ];
}
