{ config, lib, helpers, ... }:

let
  inherit (lib) mkEnableOption mkIf mkOption types;
  inherit (helpers) mkRaw;
  cfg = config.modules.remaps;
in
{
  options.modules.remaps = {
    enable = mkEnableOption "remap module";

    layout = mkOption {
      type = types.enum [ "colemak" "qwerty" ];
      default = "qwerty";
      description = "Current Keyboard layout. Options: [qwerty] colemak";
    };

    keys.undo = mkOption {
      type = types.str;
      default = "u";
      description = "key to undo";
    };
  };

  imports = [
    ./colemak.nix
    ./querty.nix
  ];

  config = mkIf cfg.enable {
    keymaps = [
      {
        mode = "v";
        key = "<";
        action = "<gv";
      }

      {
        mode = "v";
        key = ">";
        action = ">gv";
      }

      {
        mode = "n";
        key = "<ESC>";
        action = "<cmd>noh<cr><esc>";
        options = {

          remap = false;
          silent = true;
        };
      }

      {
        mode = "n";
        key = "<c-d>";
        action = "<c-d>zz";
        options = {
          remap = false;
          silent = true;
        };
      }

      {
        mode = "n";
        key = "<c-u>";
        action = "<c-u>zz";
        options = {
          remap = false;
          silent = true;
        };
      }

      (mkIf (!config.vscode.enable) {
        mode = "n";
        key = "<c-w>t";
        action = ":tab split <CR>";
        options = {
          silent = true;
          desc = "Create a new tab";
        };
      })

      {
        mode = "n";
        key = "<leader>ff";
        action = mkRaw "vim.cmd.Ex";
        options = {
          silent = true;
          noremap = true;
          desc = "open file";
        };
      }

      (mkIf (!config.vscode.enable) {
        mode = "n";
        key = "<leader>ft";
        action = mkRaw "vim.cmd.Tex";
        options = {
          silent = true;
          noremap = true;
          desc = "open file in new tab";
        };
      })

      (mkIf (!config.vscode.enable) {
        mode = "n";
        key = "<leader>fv";
        action = mkRaw "vim.cmd.Vex";
        options = {
          silent = true;
          noremap = true;
          desc = "open file in new vsplit";
        };
      })

      (mkIf (!config.vscode.enable) {
        mode = "n";
        key = "<leader>fs";
        action = mkRaw "vim.cmd.Sex";
        options = {
          silent = true;
          noremap = true;
          desc = "open file in new split";
        };
      })

      (mkIf (!config.vscode.enable) {
        mode = "n";
        key = "<leader>bo";
        action = ":browse old<CR>";
        options = {
          silent = true;
          noremap = true;
          desc = "browse oldfiles";
        };
      })

      {
        mode = "n";
        key = "<leader>w";
        action = mkRaw "vim.cmd.w";
        options = {
          silent = true;
          desc = "save";
        };
      }

      (mkIf (!config.vscode.enable) {
        mode = "n";
        key = "<leader>x";
        action = ":!chmod +x %<CR>";
        options = {
          silent = true;
          desc = "mark the current file as executable";
        };
      })

      (mkIf (!config.vscode.enable) {
        mode = "n";
        key = "<leader>z";
        action = ":!chmod -x %<CR>";
        options = {
          silent = true;
          desc = "mark the current file as non-executable";
        };
      })

      {
        mode = "n";
        key = cfg.keys.undo;
        action = "u";
      }

      (mkIf (!config.vscode.enable && config.plugins.which-key.enable) {
        mode = "n";
        key = "gt";
        action = "gt";
        options = {
          silent = true;
          noremap = true;
          desc = "Go to the next tab";
        };
      })

      (mkIf (!config.vscode.enable && config.plugins.which-key.enable) {
        mode = "n";
        key = "gT";
        action = "gT";
        options = {
          silent = true;
          noremap = true;
          desc = "Go to the previous tab";
        };
      })

      # quit
      (mkIf (!config.vscode.enable) {
        mode = "n";
        key = "ZAQ";
        action = ":qa!<CR>";
        options = {
          silent = true;
          desc = "Force quit all buffers";
        };
      })

      (mkIf (!config.vscode.enable) {
        mode = "n";
        key = "ZAZ";
        action = ":wqa!<CR>";
        options = {
          silent = true;
          desc = "Save all buffers and force quit";
        };
      })

      (mkIf (!config.vscode.enable && config.plugins.which-key.enable) {
        mode = "n";
        key = "ZZ";
        action = "ZZ";
        options = {
          silent = true;
          noremap = true;
          desc = "Save and close a window";
        };
      })

      (mkIf (!config.vscode.enable && config.plugins.which-key.enable) {
        mode = "n";
        key = "ZQ";
        action = "ZQ";
        options = {
          silent = true;
          noremap = true;
          desc = "Force close a window";
        };
      })
    ];
  };
}
