{ pkgs, config, lib, ... }:

let
  inherit (lib) mkAfter mkEnableOption mkIf;
  cfg = config.plugins.vifm;
in
{
  options.plugins.vifm = {
    enable = mkEnableOption "vifm";
  };

  config = mkIf cfg.enable {
    extraPlugins = with pkgs.vimPlugins; [ vifm-vim ];

    keymaps = mkAfter [
      {
        mode = "n";
        key = "<leader>ff";
        action = ":Vifm<CR>";
        options = {
          silent = true;
          desc = "vifm";
        };
      }

      {
        mode = "n";
        key = "<leader>fv";
        action = ":VsplitVifm<CR>";
        options = {
          silent = true;
          desc = "vifm vertical split";
        };
      }

      {
        mode = "n";
        key = "<leader>fs";
        action = ":SplitVifm<CR>";
        options = {
          silent = true;
          desc = "vifm split";
        };
      }

      {
        mode = "n";
        key = "<leader>ft";
        action = ":TabVifm<CR>";
        options = {
          silent = true;
          desc = "vifm tab";
        };
      }
    ];
  };
}
