{ config, lib, pkgs, ... }:

let
  inherit (lib) mkBefore mkDefault mkIf mkMerge;
  cfg = config.plugins.luasnip;
in
mkMerge [
  { plugins.luasnip.enable = mkDefault config.modules.snippets.enable; }

  (mkIf cfg.enable {
    plugins.cmp.settings = {
      tabNextHooks = [{
        condition = "require(\"luasnip\").expand_or_jumpable()";
        action = "require(\"luasnip\").expand_or_jump()";
      }];

      tabPrevHooks = [{
        condition = "require(\"luasnip\").jumpable(-1)";
        action = "require(\"luasnip\").jump(-1)";
      }];

      snippet.expand = "luasnip";

      sources = mkBefore [
        { name = "luasnip"; }
      ];
    };

    plugins.luasnip.fromVscode = [
      { paths = "${pkgs.vimPlugins.friendly-snippets}"; }
    ];
  })
]
