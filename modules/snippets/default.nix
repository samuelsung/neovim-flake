{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.modules.snippets.enable = mkEnableOption "snippets";

  imports = [
    ./luasnip.nix
    ./snippets.nix
  ];
}
