{ config, lib, pkgs, ... }:

let
  inherit (builtins) isString toJSON;
  inherit (lib) attrNames concatMapStringsSep mapAttrsToList mkOption splitString types;
  cfg = config.snippets.languages;

  writeJSONFile = name: obj: pkgs.writeTextFile { inherit name; text = toJSON obj; };

  package-json = {
    name = "snippets";
    contributes.snippets = mapAttrsToList
      (lang: _: {
        language = [ lang ];
        path = "./${lang}.json";
      })
      cfg;
  };

  snippets-folder = pkgs.runCommand "snippets-folder" { } ''
    mkdir $out

    ln -s "${writeJSONFile "package.json" package-json}" "$out/package.json"

    ${concatMapStringsSep "\n"
      (lang:
        let
          name = "${lang}.json";
          langJson = writeJSONFile name cfg.${lang};
        in
          ''
            ln -s "${langJson}" "$out/${name}"
          '')
      (attrNames cfg)}
  '';
in
{
  options.snippets.languages = mkOption
    {
      type = types.attrsOf (types.attrsOf (types.submodule {
        options.prefix = mkOption
          {
            type = with types; oneOf [ str (listOf str) ];
            apply = prefix: if isString prefix then [ prefix ] else prefix;
            description = ''
              prefix defines one or more trigger words that display the snippet in IntelliSense.
            '';
          };

        options.body = mkOption
          {
            type = types.lines;
            apply = splitString "\n";
            description = ''
              body is one or more lines of content,
              which will be joined as multiple lines upon insertion.
            '';
          };

        options.description = mkOption
          {
            type = types.str;
            default = "";
            description = ''
              description is an optional description of the snippet displayed by IntelliSense.
            '';
          };
      }));
      default = { };
    };

  config = {
    plugins.luasnip.fromVscode = [
      { paths = "${snippets-folder}"; }
    ];
  };
}
