{ pkgs, config, lib, ... }:
with lib;
with builtins;

let
  inherit (config.vim) scope;
  cfg = config.vim.snippets.vsnip;
in
{
  options.vim.snippets.vsnip = {
    enable = mkEnableOption "Enable vim-vsnip";
  };

  config = mkIf (cfg.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [ vim-vsnip ];
  };
}
