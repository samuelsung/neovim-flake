{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf mkMerge;
in
{
  options.page.enable = mkEnableOption "page";
  options.vscode.enable = mkEnableOption "vscode";

  config = mkMerge [
    {
      modules.remaps.enable = true;
      modules.remaps.layout = "colemak";
      globals.editorconfig = false;
      globals.mapleader = " ";
      globals.maplocalleader = " ";
      clipboard.register = "unnamedplus";

      autoChecktime = true;
      disableExModeBinding = true;
      resizeSplitOnResize = true;
      createDirectoryIfNotExist = true;
      backToLastLocation = true;

      opts.encoding = "utf-8";
      opts.ffs = "unix,dos,mac";
      opts.scrolloff = 10;
      opts.cmdheight = 1;
      opts.updatetime = 300;
      # timeout in ms that neovim will wait for mapped action to complete
      opts.tm = 500;
      opts.hidden = true;
      opts.wrap = true;

      opts.undofile = true;
      opts.undolevels = 100000;

      opts.swapfile = false;
      opts.backup = false;
      opts.writebackup = false;

      opts.expandtab = true;
      opts.smartindent = true; # Insert indents automatically
      opts.tabstop = 4;
      opts.softtabstop = 4;
      opts.shiftround = true; # Round indent
      opts.shiftwidth = 4;
      opts.autoindent = true;

      opts.mouse = "nv";
      opts.confirm = true; # Confirm to save changes before exiting modified buffer

      opts.smartcase = true; # Don't ignore case with capitals
      opts.inccommand = "nosplit"; # preview incremental substitute
      opts.ignorecase = true;

      opts.completeopt = "menu,menuone,noselect";
      opts.spelllang = [ "en" ];

      opts.errorbells = false;
      opts.visualbell = false;

      opts.splitbelow = true;
      opts.splitright = true;

      plugins.treesitter.enable = true;
      plugins.undotree.enable = !config.vscode.enable;
      plugins.which-key.enable = !config.vscode.enable;

      plugins.tmux-navigator.enable = !config.vscode.enable;
      plugins.tmux-navigator.settings.no_mappings = 1;

      plugins.todo-comments.enable = !config.vscode.enable;

      modules.visuals.enable = !config.vscode.enable;
    }

    (mkIf (!config.page.enable) {
      plugins.harpoon.enable = !config.vscode.enable;
      plugins.yazi.enable = !config.vscode.enable;
      plugins.neorg.enable = !config.vscode.enable;
      plugins.telescope.enable = !config.vscode.enable;
      plugins.trouble.enable = !config.vscode.enable;
      plugins.diffview.enable = !config.vscode.enable;

      modules.autopairs.enable = true;
      modules.commenting.enable = true;
      modules.completion.enable = !config.vscode.enable;
      modules.git.enable = true;
    })
  ];

  imports = [
    ./autopairs
    ./basic
    ./commenting
    ./completion
    ./debugger
    ./diffview
    ./flash
    ./git
    ./harpoon
    ./languages
    ./lsp
    ./packages.nix
    ./remaps
    ./snippets
    ./telescope
    ./treesitter
    ./trouble
    ./undotree
    ./vifm
    ./visuals
    ./yanky
    ./yazi
    ../plugins
  ];
}
