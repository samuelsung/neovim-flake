{ config, lib, pkgs, ... }:
{
  imports = [
    ./completion
    ./theme
    ./core
    ./basic
    ./statusline
    ./tabline
    ./filetree
    ./visuals
    ./lsp
    ./treesitter
    ./autopairs
    ./snippets
    ./keyboardLayout
    ./keys
    ./markdown
    ./telescope
    ./git
    ./commenting
    ./trouble
    ./scopes
    ./nvimpager
  ];
}
