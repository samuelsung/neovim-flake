{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.modules.lsp.enable = mkEnableOption "lsp support";

  imports = [
    ./lsp-format.nix
    ./lsp.nix
  ];
}
