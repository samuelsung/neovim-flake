{ pkgs, config, lib, ... }:
with lib;
with builtins;

let
  inherit (config.vim) scope;
  cfg = config.vim.lsp;
in
{

  options.vim.lsp = {
    lspSignature.enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable lsp signature viewer";
    };
  };

  config = mkIf (cfg.enable && cfg.lspSignature.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [ lsp-signature ];

    vim.luaConfigRC = ''
      -- Enable lsp signature viewer
      require("lsp_signature").setup()
    '';
  };
}
