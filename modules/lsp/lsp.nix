{ pkgs, config, lib, utils, ... }:

with lib;
with builtins;
with utils;

let
  inherit (config.vim) scope;
  cfg = config.vim.lsp;
in
{
  options.vim.lsp = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable neovim lsp support";
    };

    enableAllLanguage = mkOption {
      type = types.bool;
      default = false;
      description = "Enable All language LSP";
    };

    sh.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable Shell LSP";
    };

    nix.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable Nix LSP";
    };

    rust = {
      enable = mkOption {
        type = types.bool;
        default = cfg.enableAllLanguage;
        description = "Enable Rust LSP";
      };

      rustAnalyzerOpts = mkOption {
        type = types.str;
        default = ''
          ["rust-analyzer"] = {
            experimental = {
              procAttrMacros = true,
            },
          },
        '';
        description = "options to pass to rust analyzer";
      };
    };

    python.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable Python LSP";
    };

    js.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable Javascript/Typescript LSP";
    };

    ts.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable Typescript LSP";
    };

    css.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable CSS LSP";
    };

    html.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable HTML LSP";
    };

    java.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable Java LSP";
    };

    clang.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable C language LSP";
    };

    sql.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable SQL language LSP";
    };

    latex.enable = mkOption {
      type = types.bool;
      default = cfg.enableAllLanguage;
      description = "Enable LaTeX language LSP";
    };
  };

  config = mkIf (cfg.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [
      nvim-lspconfig
      null-ls
      (if (config.vim.autocomplete.enable && (config.vim.autocomplete.type == "nvim-cmp")) then cmp-nvim-lsp else null)
      (if cfg.sql.enable then sqls-nvim else null)
      (if cfg.java.enable then nvim-jdtls else null)
    ] ++ (if cfg.rust.enable then [
      crates-nvim
      rust-tools
    ] else [ ]);

    vim.nnoremap = {
      "<leader>tf" = "<cmd>lua _G.toggle_save_format()<CR>";
    };

    vim.configRC = ''
      ${if cfg.rust.enable then ''
        function! MapRustTools()
          nnoremap <silent>ri <cmd>lua require('rust-tools.inlay_hints').toggle_inlay_hints()<CR>
          nnoremap <silent>rr <cmd>lua require('rust-tools.runnables').runnables()<CR>
          nnoremap <silent>re <cmd>lua require('rust-tools.expand_macro').expand_macro()<CR>
          nnoremap <silent>rc <cmd>lua require('rust-tools.open_cargo_toml').open_cargo_toml()<CR>
          nnoremap <silent>rg <cmd>lua require('rust-tools.crate_graph').view_crate_graph('x11', nil)<CR>
        endfunction

        autocmd filetype rust nnoremap <silent>ri <cmd>lua require('rust-tools.inlay_hints').toggle_inlay_hints()<CR>
        autocmd filetype rust nnoremap <silent>rr <cmd>lua require('rust-tools.runnables').runnables()<CR>
        autocmd filetype rust nnoremap <silent>re <cmd>lua require('rust-tools.expand_macro').expand_macro()<CR>
        autocmd filetype rust nnoremap <silent>rc <cmd>lua require('rust-tools.open_cargo_toml').open_cargo_toml()<CR>
        autocmd filetype rust nnoremap <silent>rg <cmd>lua require('rust-tools.crate_graph').view_crate_graph('x11', nil)<CR>
      '' else ""}

      ${if cfg.sh.enable then ''
        autocmd filetype sh setlocal tabstop=2 shiftwidth=2 softtabstop=2
      '' else ""}

      ${if cfg.nix.enable then ''
        autocmd filetype nix setlocal tabstop=2 shiftwidth=2 softtabstop=2
      '' else ""}

      ${if cfg.js.enable then ''
        autocmd filetype javascript,javascriptreact setlocal tabstop=2 shiftwidth=2 softtabstop=2
      '' else ""}

      ${if cfg.ts.enable then ''
        autocmd filetype typescript,typescriptreact setlocal tabstop=2 shiftwidth=2 softtabstop=2
      '' else ""}

      ${if cfg.clang.enable then ''
        " c syntax for header (otherwise breaks treesitter highlighting)
        " https://www.reddit.com/r/neovim/comments/orfpcd/question_does_the_c_parser_from_nvimtreesitter/
        let g:c_syntax_for_h = 1
      '' else ""}
    '';

    vim.luaConfigRC = ''
      local null_ls = require("null-ls")
      local null_helpers = require("null-ls.helpers")
      local null_methods = require("null-ls.methods")

      local ls_sources = {
        ${writeIf cfg.sh.enable ''
          null_ls.builtins.diagnostics.shellcheck.with({
            command = "${pkgs.shellcheck}/bin/shellcheck",
          }),

          null_ls.builtins.formatting.shfmt.with({
            command = "${pkgs.shfmt}/bin/shfmt",
            extra_args = { "-ci", "-s", "-bn", "-i", "2" },
          }),
        ''}

        ${writeIf cfg.python.enable ''
          null_ls.builtins.formatting.black.with({
            command = "${pkgs.black}/bin/black",
          }),
        ''}

        ${writeIf cfg.js.enable ''
          null_ls.builtins.formatting.eslint_d.with({
            command = "${pkgs.nodePackages.eslint_d}/bin/eslint_d"
          }),
          null_ls.builtins.diagnostics.eslint_d.with({
            command = "${pkgs.nodePackages.eslint_d}/bin/eslint_d"
          }),
          null_ls.builtins.code_actions.eslint_d.with({
            command = "${pkgs.nodePackages.eslint_d}/bin/eslint_d"
          }),
        ''}

        ${writeIf (cfg.sql.enable && false /* TODO(samuelsung): sqlfluff seems to be broken on unstable (https://github.com/NixOS/nixpkgs/pull/173899), reenable it when the patch got merged into unstable */) ''
          null_helpers.make_builtin({
            method = null_methods.internal.FORMATTING,
            filetypes = { "sql" },
            generator_opts = {
              to_stdin = true,
              ignore_stderr = true,
              suppress_errors = true,
              command = "${pkgs.sqlfluff}/bin/sqlfluff",
              args = {
                "fix",
                "-",
              },
            },
            factory = null_helpers.formatter_factory,
          }),

          null_helpers.make_builtin({
            method = null_methods.internal.DIAGNOSTICS,
            filetypes = { "sql" },
            generator_opts = {
              command = "${pkgs.sqlfluff}/bin/sqlfluff",
              args = {
                "lint",
                "--format",
                "json",
                "-",
              },
              to_stdin = true,
              from_stderr = true,
              format = "json",
              on_output = function(params)
                params.messages = params and params.output and params.output[1] and params.output[1].violations or {}

                local diagnostics = {}
                for _, json_diagnostic in ipairs(params.messages) do
                  local diagnostic = {
                    row = json_diagnostic["line_no"],
                    col = json_diagnostic["line_pos"],
                    code = json_diagnostic["code"],
                    message = json_diagnostic["description"],
                    severity = null_helpers.diagnostics.severities["information"],
                  }

                  table.insert(diagnostics, diagnostic)
                end

                return diagnostics
              end,
            },
            factory = null_helpers.generator_factory,
          })
        ''}
      }

      -- Enable formatting
      vim.g.save_format_state = false;

      _G.toggle_save_format = function()
        vim.g.save_format_state = not vim.g.save_format_state;
      end;

      _G.check_formatting_sync = function()
        if (vim.g.save_format_state) then
          vim.lsp.buf.formatting_sync()
        end
      end;

      save_format = function(client)
        if client.resolved_capabilities.document_formatting then
          vim.cmd("autocmd BufWritePost <buffer> lua _G.check_formatting_sync()")
        end
      end

      default_on_attach = function(client)
        save_format(client)
      end

      -- Enable null-ls
      null_ls.setup({
        diagnostics_format = "[#{m}] #{s} (#{c})",
        debounce = 250,
        default_timeout = 5000,
        sources = ls_sources,
        on_attach = default_on_attach,
        debug = true,
      })

      -- Enable lspconfig
      local lspconfig = require('lspconfig')

      local capabilities = vim.lsp.protocol.make_client_capabilities()
      ${if config.vim.autocomplete.enable then (if config.vim.autocomplete.type == "nvim-compe" then ''
        vim.capabilities.textDocument.completion.completionItem.snippetSupport = true
        capabilities.textDocument.completion.completionItem.resolveSupport = {
          properties = {
            'documentation',
            'detail',
            'additionalTextEdits',
          }
        }
      '' else ''
        capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)
      '') else ""}

      ${writeIf cfg.rust.enable ''
        -- Rust config

        local rustopts = {
          server = {
            capabilities = capabilities,
            on_attach = default_on_attach,
            cmd = {"${pkgs.rust-analyzer}/bin/rust-analyzer"},
            settings = {
              ${cfg.rust.rustAnalyzerOpts}
            }
          }
        }

        lspconfig.rust_analyzer.setup{}
        require('crates').setup{}
        require('rust-tools').setup(rustopts)
        require('rust-tools.inlay_hints').set_inlay_hints()
      ''}

      ${writeIf cfg.python.enable ''
        -- Python config
        lspconfig.pyright.setup{
          capabilities = capabilities;
          on_attach=default_on_attach;
          cmd = {"${pkgs.nodePackages.pyright}/bin/pyright-langserver", "--stdio"}
        }
      ''}

      ${writeIf cfg.nix.enable ''
        -- Nix config
        lspconfig.rnix.setup{
          capabilities = capabilities;
          on_attach=default_on_attach;
          cmd = {"${pkgs.rnix-lsp}/bin/rnix-lsp"}
        }
      ''}

      ${writeIf
        # ISSUE(samuelsung): one dependency of ccls (libcxx) has been marked an broken on darwin
        # TODO(samuelsung): unmark isDarwin when the issue got fixed.
        # However, I don't use mac anyway 🤠
        # SEE: https://github.com/NixOS/nixpkgs/pull/147289
        (cfg.clang.enable && !pkgs.stdenv.isDarwin)
        ''
          -- CCLS (clang) config
          lspconfig.ccls.setup{
            cmd = {"${pkgs.ccls}/bin/ccls"}
          }
        ''}

      ${writeIf cfg.ts.enable ''
        lspconfig.tsserver.setup({
          capabilities = capabilities;
          on_attach = function(client)
            client.resolved_capabilities.document_formatting = false
            client.resolved_capabilities.document_range_formatting = false
          end,
          cmd = { '${pkgs.nodePackages.typescript-language-server}/bin/typescript-language-server', '--stdio' },
          filetypes = { 'javascript', 'javascriptreact',
            'javascript.jsx',
            'typescript',
            'typescriptreact',
            'typescript.tsx',
          },
          root_dir = require('lspconfig/util').root_pattern(
            'package.json',
            'tsconfig.json',
            'jsconfig.json',
            '.git'
          ),
          init_options = { formatting = false },
        });
      ''}

      ${writeIf cfg.css.enable ''
        lspconfig.cssls.setup({
          capabilities = capabilities;
          cmd = { '${pkgs.nodePackages.vscode-langservers-extracted}/bin/vscode-css-language-server', '--stdio' },
        });
      ''}

      ${writeIf cfg.html.enable ''
        lspconfig.html.setup({
          capabilities = capabilities;
          cmd = { '${pkgs.nodePackages.vscode-langservers-extracted}/bin/vscode-html-language-server', '--stdio' },
        });
      ''}

      ${writeIf cfg.java.enable ''
          vim.cmd([[
            augroup lsp
              au!
              au FileType java lua _G.attach_jdtls()
            augroup end
          ]])

          _G.attach_jdtls = function()
            require('jdtls').start_or_attach({
              -- on_attach = on_attach,
              cmd = { "${pkgs.jdt-language-server}/bin/jdt-language-server" },
              root_dir = require('jdtls.setup').find_root({'build.gradle', 'pom.xml', '.git'}),
              -- init_options = {bundles = bundles}
            });
          end;
      ''}

      ${writeIf cfg.sql.enable ''
        -- SQLS config
        lspconfig.sqls.setup {
          on_attach = function(client)
            client.resolved_capabilities.execute_command = true
            -- use null-ls with sqlfluff instead
            client.resolved_capabilities.document_formatting = false

            require'sqls'.setup{}
          end,
          cmd = {"${pkgs.sqls}/bin/sqls", "-config", string.format("%s/config.yml", vim.fn.getcwd()) }
        }
      ''}

      ${writeIf cfg.latex.enable ''
        lspconfig.texlab.setup({
          settings = {
            bibtex = {
              formatting = {
                lineLength = 120
              }
            },
            latex = {
              build = {
                args = { "-pdf", "-interaction=nonstopmode", "-synctex=1", "%f" },
                executable = "latexmk",
                onSave = false
              },
              forwardSearch = {
                args = {},
                onSave = false
              },
              lint = {
                onChange = true
              }
            }
          }
        });
      ''}
    '';
  };
}
