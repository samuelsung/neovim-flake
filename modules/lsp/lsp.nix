{ config, lib, helpers, ... }:

let
  inherit (helpers) mkRaw;
  inherit (lib) mkAfter mkDefault mkIf mkMerge mkOrder;
  cfg = config.plugins.lsp;
in
mkMerge [
  { plugins.lsp.enable = mkDefault config.modules.lsp.enable; }

  (mkIf cfg.enable {
    # disable highlight from lsp
    plugins.lsp.capabilities = ''
      capabilities.semanticTokensProvider = nil
    '';

    plugins.lspkind.enable = true;
    plugins.lspkind.extraOptions.before = mkRaw ''
      function(entry,vim_item)
        vim_item.menu = ({
          buffer = "[Buffer]",
          rg = "[RG]",
          nvim_lsp = "[LSP]",
          vsnip = "[VSnip]",
          luasnip = "[LuaSnip]",
          crates = "[Crates]",
          path = "[Path]",
          tmux = "[TMUX]",
          git = "[GIT]",
          calc = "[CALC]",
          zsh = "[ZSH]",
          npm = "[NPM]",
        })[entry.source.name]

        return vim_item;
      end
    '';

    plugins.cmp.settings.sources = mkOrder 100 [
      { name = "nvim_lsp"; }
    ];

    plugins.lsp.keymaps = {
      silent = true;
    };

    keymaps = mkAfter [
      {
        mode = "n";
        key = "E";
        action = mkRaw "vim.lsp.buf.hover";
        options = {
          silent = true;
        };
      }

      {
        mode = "n";
        key = "[d";
        action = mkRaw "vim.diagnostic.goto_prev";
        options = {
          silent = true;
          desc = "Previous lsp diagnostics";
        };
      }

      {
        mode = "n";
        key = "]d";
        action = mkRaw "vim.diagnostic.goto_next";
        options = {
          silent = true;
          desc = "Next lsp diagnostics";
        };
      }

      {
        mode = "n";
        key = "gd";
        action = mkRaw "vim.lsp.buf.definition";
        options = {
          silent = true;
          desc = "Go to lsp definition";
        };
      }

      # lsp references
      {
        mode = "n";
        key = "glr";
        action = mkRaw "vim.lsp.buf.references";
        options = {
          silent = true;
          desc = "Go to lsp references";
        };
      }

      # lsp types
      {
        mode = "n";
        key = "glt";
        action = mkRaw "vim.lsp.buf.type_definition";
        options = {
          silent = true;
          desc = "Go to lsp type definition";
        };
      }

      # lsp implementations
      {
        mode = "n";
        key = "gli";
        action = mkRaw "vim.lsp.buf.implementation";
        options = {
          silent = true;
          desc = "Go to lsp implementation";
        };
      }

      {
        mode = "n";
        key = "glc";
        action = mkRaw "vim.lsp.buf.code_action";
        options = {
          silent = true;
          desc = "Get code action";
        };
      }
    ];
  })
]
