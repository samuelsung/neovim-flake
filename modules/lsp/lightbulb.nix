{ pkgs, config, lib, ... }:
with lib;
with builtins;

let
  inherit (config.vim) scope;
  cfg = config.vim.lsp;
in
{

  options.vim.lsp = {
    trouble = {
      enable = mkEnableOption "trouble diagnostics viewer";
    };
  };

  config = mkIf (cfg.enable && cfg.trouble.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [ nvim-lightbulb ];

    vim.luaConfigRC = ''
      -- Enable trouble diagnostics viewer
      require("trouble").setup {}
    '';
  };
}
