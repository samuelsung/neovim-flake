{ config, lib, ... }:
let
  inherit (lib) mkDefault mkIf mkMerge;
  cfg = config.plugins.lsp-format;
in
mkMerge [
  { plugins.lsp-format.enable = mkDefault config.modules.lsp.enable; }

  (mkIf cfg.enable {
    plugins.none-ls.enableLspFormat = true;
  })
]
