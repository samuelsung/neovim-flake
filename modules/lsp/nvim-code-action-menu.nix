{ pkgs, config, lib, ... }:
with lib;
with builtins;

let
  inherit (config.vim) scope;
  cfg = config.vim.lsp;
in
{
  options.vim.lsp = {
    nvimCodeActionMenu.enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable nvim code action menu";
    };
  };

  config = mkIf (cfg.enable && cfg.nvimCodeActionMenu.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [
      nvim-code-action-menu
    ];

    vim.nnoremap = {
      "<silent><leader>ca" = ":CodeActionMenu<CR>";
    };
  };
}
