{ config, lib, utils, ... }:

with lib;
with builtins;
with utils;

let
  inherit (config.vim) scope;
  cfg = config.vim.nvimpager;
in
{
  options.vim.nvimpager = {
    overrideMapping = mkOption {
      type = types.bool;
      default = true;
      description = "Enable overriding the default mapping from nvimpager";
    };
  };

  config = mkIf (scope == "nvimpager") {
    # Disable mappings come with vim
    vim.globals = mkIf (cfg.overrideMapping) {
      no_man_maps = "v:true";
    };

    # Disable mappings come with nvimpager
    vim.luaConfigRC = mkIf (cfg.overrideMapping) ''
      if nvimpager ~= nil then
        nvimpager.maps = false
      end
    '';
  };
}
