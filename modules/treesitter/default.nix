{ config, lib, ... }:

let
  inherit (lib) mkDefault mkIf;
in
{
  plugins.ts-autotag.enable = mkDefault
    (config.modules.autopairs.enable && config.plugins.treesitter.enable);

  plugins.ts-context-commentstring = {
    enable = mkDefault (config.modules.commenting.enable && config.plugins.treesitter.enable);
    extraOptions.enable_autocmd = mkDefault false;
    skipTsContextCommentStringModule = true;
  };
  plugins.comment.settings.pre_hook = mkIf config.plugins.ts-context-commentstring.enable
    "require('ts_context_commentstring.integrations.comment_nvim').create_pre_hook()";

  plugins.treesitter.settings.highlight.enable = true;

  plugins.treesitter.settings.incremental_selection = {
    enable = true;
    keymaps = {
      init_selection = "<C-E>";
      node_incremental = "<C-E>";
      # scope_incremental = "<C-E>";
      node_decremental = "<C-N>";
    };
  };

  plugins.treesitter.settings.indent.enable = true;

  opts = {
    foldmethod = "expr";
    foldexpr = "nvim_treesitter#foldexpr()";
    foldenable = false;
  };
}
