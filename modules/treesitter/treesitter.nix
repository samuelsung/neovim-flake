{ pkgs, config, lib, utils, ... }:

with lib;
with builtins;
with utils;

let
  cfg = config.vim.treesitter;
in
{
  options.vim.treesitter = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "enable tree-sitter [nvim-treesitter]";
    };

    fold = mkOption {
      type = types.bool;
      default = false;
      description = "enable fold with tree-sitter";
    };

    comment = mkOption {
      type = types.bool;
      default = true;
      description = "enable nvim-ts-context-commentstring";
    };

    autotagHtml = mkOption {
      type = types.bool;
      default = true;
      description = "enable autoclose and rename html tag [nvim-ts-autotag]";
    };
  };

  config = mkIf cfg.enable {

    vim.startPlugins = with pkgs.neovimPlugins;
      [
        (pkgs.vimPlugins.nvim-treesitter.overrideAttrs (prev:
          let
            treesitterGrammars = pkgs.tree-sitter.withPlugins (_: pkgs.tree-sitter.allGrammars);
          in
          {
            postPatch = ''
              rm -r parser
              ln -s ${treesitterGrammars} parser
            '';
          }))
        (if cfg.autotagHtml then nvim-ts-autotag else null)
        (if cfg.comment then nvim-ts-context-commentstring else null)
      ];

    vim.configRC = writeIf cfg.fold ''
      " Tree-sitter based folding
      set foldmethod=expr
      set foldexpr=nvim_treesitter#foldexpr()
      set nofoldenable
    '';

    vim.luaConfigRC = ''
      -- Treesitter config
      require'nvim-treesitter.configs'.setup {
        highlight = {
          enable = true,
          disable = {},
        },

        incremental_selection = {
          enable = true,
          keymaps = {
            init_selection = "<C-E>",
            node_incremental = "<C-E>",
            -- scope_incremental = "<C-E>",
            node_decremental = "<C-N>",
          },
        },

        ${writeIf cfg.autotagHtml ''
          autotag = {
            enable = true,
          },
        ''}

        ${writeIf cfg.comment ''
          context_commentstring = {
            enable = true,
            enable_autocmd = false,

            config = {
              -- nix = '# %s',
              sql = '-- %s',
              mysql = '-- %s',
            },
          },
        ''}
      }
    '';
  };
}
