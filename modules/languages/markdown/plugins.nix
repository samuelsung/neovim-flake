{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.markdown.plugins;
in
{
  options.languages.markdown.plugins.enable = mkEnableOption "plugins for markdown";

  config = mkIf cfg.enable {
    languages.maps.markdown.normal."<leader>p" = {
      action = ":Glow<CR>";
      desc = "Glow preview";
    };

    plugins.glow.enable = true;
    plugins.glow.settings.border = "none";
  };
}
