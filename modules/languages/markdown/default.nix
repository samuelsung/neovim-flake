{
  languages.bo.markdown = {
    tabstop = 4;
    shiftwidth = 4;
    softtabstop = 4;
  };

  languages.wo.markdown = {
    wrap = true;
    spell = true;
  };

  imports = [
    ./plugins.nix
  ];
}
