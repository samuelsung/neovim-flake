{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.spelling.plugins;
in
{
  options.languages.spelling.plugins.enable = mkEnableOption "plugins for spell checking";

  config = mkIf cfg.enable {
    plugins.lsp.servers.ltex.enable = true;
  };
}
