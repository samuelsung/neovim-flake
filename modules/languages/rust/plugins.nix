{ config, helpers, lib, pkgs, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  inherit (helpers) mkRaw;
  cfg = config.languages.rust.plugins;
in
{
  options.languages.rust.plugins.enable = mkEnableOption "plugins for rust";

  config = mkIf cfg.enable {
    plugins.rustaceanvim = {
      enable = true;
      settings.server = { };
      settings.tools = {
        on_initialized = ''
          function()
            vim.cmd([[
              augroup RustLSP
                autocmd BufEnter,CursorHold,InsertLeave *.rs silent! lua vim.lsp.codelens.refresh()
              augroup END
            ]])
          end
        '';
      };
      settings.dap = {
        adapter =
          let
            codelldb_path = "${pkgs.vscode-extensions.vadimcn.vscode-lldb}/share/vscode/extensions/vadimcn.vscode-lldb/adapter/codelldb";
            liblldb_path = "${pkgs.vscode-extensions.vadimcn.vscode-lldb}/share/vscode/extensions/vadimcn.vscode-lldb/lldb/lib/liblldb.so";
          in
          mkRaw ''
            require("rustaceanvim.config").get_codelldb_adapter("${codelldb_path}", "${liblldb_path}")
          '';
      };
    };

    extraPackages = [ pkgs.rustfmt ];

    plugins.cmp.settings.sources = [
      { name = "crates"; }
    ];

    languages.maps.rust.normal = {
      "<leader>rr" = {
        action = "function() vim.cmd.RustLsp('runnables') end";
        lua = true;
        desc = "Runnables";
      };

      "<leader>re" = {
        action = "function() vim.cmd.RustLsp('expandMacro') end";
        lua = true;
        desc = "Expand macro";
      };

      "<leader>rc" = {
        action = "function() vim.cmd.RustLsp('openCargo') end";
        lua = true;
        desc = "Open cargo toml";
      };

      "<leader>rg" = {
        action = ''
          function()
            vim.cmd.RustLsp { 'crateGraph', 'x11', nil }
          end
        '';
        lua = true;
        desc = "Create graph";
      };
    };
  };
}
