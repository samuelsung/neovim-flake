{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.css.plugins;
in
{
  options.languages.css.plugins.enable = mkEnableOption "plugins for css";

  config = mkIf cfg.enable {
    plugins.lsp.servers.cssls.enable = true;
    plugins.lsp.servers.tailwindcss.enable = true;
  };
}
