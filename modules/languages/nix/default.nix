{
  languages.bo.nix = {
    tabstop = 2;
    shiftwidth = 2;
    softtabstop = 2;
  };

  languages.wo.nix = {
    colorcolumn = "100";
  };

  imports = [
    ./plugins.nix
    ./snippets.nix
  ];
}
