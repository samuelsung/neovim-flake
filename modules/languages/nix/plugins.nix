{ config, lib, ... }:

let
  inherit (lib) mkDefault mkEnableOption mkIf;
  cfg = config.languages.nix.plugins;
in
{
  options.languages.nix.plugins.enable = mkEnableOption "plugins for nix";

  config = mkIf cfg.enable {
    plugins.nix.enable = true;

    plugins.lsp.servers.nil_ls.enable = true;

    plugins.none-ls = {
      enable = mkDefault config.plugins.lsp.enable;
      sources.diagnostics.deadnix.enable = true;
      sources.diagnostics.statix.enable = true;
      sources.formatting.nixpkgs_fmt.enable = true;
    };
  };
}
