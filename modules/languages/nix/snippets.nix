{
  snippets.languages.nix = {
    module = {
      prefix = "module";
      body = ''
        { $1, ... }:

        {
          $2
        }
      '';
    };

    "inherit" = {
      prefix = "inherit";
      body = ''
        inherit ($1) $2;
      '';
    };
  };
}
