{
  languages.bo.gitcommit = {
    tabstop = 4;
    shiftwidth = 4;
    softtabstop = 4;
  };

  languages.wo.gitcommit = {
    wrap = true;
    spell = true;
  };
}
