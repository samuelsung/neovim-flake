{ config, lib, ... }:

let
  inherit (lib) optionalString;
in
{
  languages.bo.sql = {
    tabstop = 2;
    shiftwidth = 2;
    softtabstop = 2;
  };

  languages.bo.mysql = {
    tabstop = 2;
    shiftwidth = 2;
    softtabstop = 2;
  };

  languages.bo.pgsql = {
    tabstop = 2;
    shiftwidth = 2;
    softtabstop = 2;
  };

  filetype.extension.pgsql = "pgsql";

  plugins.web-devicons.customIcons.mysql = {
    icon = "";
    color = "#dad8d8";
    cterm_color = "188";
    name = "Sql";
  };

  plugins.web-devicons.customIcons.pgsql = {
    icon = "";
    color = "#dad8d8";
    cterm_color = "188";
    name = "Sql";
  };

  plugins.ts-context-commentstring.extraOptions.languages = {
    sql = "-- %s";
    mysql = "-- %s";
    pgsql = "-- %s";
  };

  extraConfigLua = ''
    ${optionalString config.plugins.treesitter.enable ''
      vim.treesitter.language.register('sql','mysql')
      vim.treesitter.language.register('sql','pgsql')
    ''}
  '';

  imports = [
    ./plugins.nix
  ];
}
