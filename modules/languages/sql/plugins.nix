{ config, pkgs, helpers, lib, ... }:

let
  inherit (helpers) mkRaw;
  inherit (lib) attrNames concatMapStringsSep mkDefault mkEnableOption mkIf mkOption types;
  cfg = config.languages.sql.plugins;
in
{
  options.languages.sql.plugins.enable = mkEnableOption "plugins for sql";

  options.languages.sql.plugins.sqlfluff = {
    dialects = mkOption {
      type = types.submodule {
        freeformType = with types; attrsOf str;

        options.sql = mkOption {
          type = types.str;
          default = "ansi";
        };
      };
      default = { };
    };
  };

  config = mkIf cfg.enable {
    languages.sql.plugins.sqlfluff.dialects = {
      mysql = "mysql";
      pgsql = "postgres";
    };

    plugins.none-ls = {
      enable = mkDefault config.plugins.lsp.enable;
      settings.sources = map
        (type: mkRaw ''
          require('null-ls').builtins.${type}.sqlfluff.with({
            command = "${pkgs.sqlfluff}/bin/sqlfluff",
            filetypes = ${helpers.toLuaObject (attrNames cfg.sqlfluff.dialects)},
            extra_args = function()
              local nixvim_dialects = ${helpers.toLuaObject cfg.sqlfluff.dialects}
              return {
                '--config',
                '${pkgs.writeTextFile {
                  name = ".sqlfluff";
                  text = ''
                    [sqlfluff]
                    sql_file_exts = ${concatMapStringsSep ","
                      (f: ".${f}") (attrNames cfg.sqlfluff.dialects)}
                  '';
                }}',
                '--dialect',
                (nixvim_dialects[vim.bo.filetype] or nixvim_dialects.sql)
              }
            end
          })
        '')
        [ "formatting" "diagnostics" ];
    };
  };
}
