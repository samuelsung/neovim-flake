{ config, helpers, lib, ... }:

let
  inherit (lib) mapAttrsToList mkOption types;
  inherit (helpers) mkRaw;
  cfg = config.languages;
in
{
  options.languages = {
    bo = mkOption {
      type = with types; attrsOf (attrsOf anything);
      default = { };
      description = ''
        buffer local options for a language.
      '';
    };

    wo = mkOption {
      type = with types; attrsOf (attrsOf anything);
      default = { };
      description = ''
        window local options for a language.
      '';
    };
  };

  config = {
    autoCmd = (
      mapAttrsToList
        (name: options:
          {
            event = [ "FileType" ];
            pattern = [ name ];
            callback = mkRaw ''
              function()
                local nixvim_options = ${helpers.toLuaObject options}

                for k,v in pairs(nixvim_options) do
                  vim.bo[k] = v
                end
              end
            '';
          })
        cfg.bo
    ) ++ (
      mapAttrsToList
        (name: options:
          {
            event = [ "FileType" ];
            pattern = [ name ];
            callback = mkRaw ''
              function()
                local winid = vim.api.nvim_get_current_win()
                local nixvim_options = ${helpers.toLuaObject options}

                for k,v in pairs(nixvim_options) do
                  vim.api.nvim_set_option_value(
                    k, v, { scope = 'local', win = winid }
                  )
                end
              end
            '';
          })
        cfg.wo
    );
  };
}
