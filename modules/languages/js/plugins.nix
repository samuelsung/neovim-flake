{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.js.plugins;
in
{
  options.languages.js.plugins.enable = mkEnableOption "plugins for js/ts";

  config = mkIf cfg.enable {
    plugins.cmp.settings.sources = [
      { name = "npm"; }
    ];

    plugins.lsp.servers.astro.enable = true;

    plugins.lsp.servers.eslint.enable = true;

    plugins.lsp.servers.ts_ls.enable = true;

    plugins.lsp.servers.ts_ls.onAttach.function = ''
      client.resolved_capabilities.document_formatting = false
      client.resolved_capabilities.document_range_formatting = false
    '';

    plugins.lsp.servers.ts_ls.rootDir = ''
      require('lspconfig/util').root_pattern(
        'package.json',
        'tsconfig.json',
        'jsconfig.json',
        '.git'
      )
    '';

    plugins.lsp.servers.ts_ls.extraOptions.init_options.formatting = false;
  };
}
