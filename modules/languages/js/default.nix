{
  languages.bo = {
    astro = {
      tabstop = 2;
      shiftwidth = 2;
      softtabstop = 2;
    };

    javascript = {
      tabstop = 2;
      shiftwidth = 2;
      softtabstop = 2;
    };

    javascriptreact = {
      tabstop = 2;
      shiftwidth = 2;
      softtabstop = 2;
    };

    typescript = {
      tabstop = 2;
      shiftwidth = 2;
      softtabstop = 2;
    };

    typescriptreact = {
      tabstop = 2;
      shiftwidth = 2;
      softtabstop = 2;
    };
  };

  imports = [
    ./plugins.nix
  ];
}
