{
  languages.bo.rasi = {
    tabstop = 2;
    shiftwidth = 2;
    softtabstop = 2;
  };

  filetype.extension.rasi = "rasi";
}
