{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.latex.plugins;
in
{
  options.languages.latex.plugins.enable = mkEnableOption "plugins for latex";

  config = mkIf cfg.enable {
    plugins.lsp.servers.texlab.enable = true;
    plugins.lsp.servers.texlab.extraOptions.settings = {
      bibtex.formatting.lineLength = 120;
      latex = {
        build = {
          args = [ "-pdf" "-interaction=nonstopmode" "-synctex=1" "%f" ];
          executable = "latexmk";
          onSave = false;
        };
        forwardSearch = {
          args = [ ];
          onSave = false;
        };
        lint.onChange = true;
      };
    };
  };
}
