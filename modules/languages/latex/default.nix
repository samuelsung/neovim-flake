{
  languages.bo.latex = {
    tabstop = 4;
    shiftwidth = 4;
    softtabstop = 4;
  };

  imports = [
    ./plugins.nix
  ];
}
