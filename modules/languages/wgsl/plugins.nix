{ config, lib, pkgs, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.wgsl.plugins;
in
{
  options.languages.wgsl.plugins.enable = mkEnableOption "plugins for wgsl";

  config = mkIf cfg.enable {
    plugins.lsp.servers.wgsl_analyzer.enable = true;
    plugins.lsp.servers.wgsl_analyzer.package = pkgs.wgsl-analyzer;
  };
}
