{
  languages.bo.wgsl = {
    tabstop = 4;
    shiftwidth = 4;
    softtabstop = 4;
  };

  filetype.extension.wgsl = "wgsl";

  imports = [
    ./plugins.nix
  ];
}
