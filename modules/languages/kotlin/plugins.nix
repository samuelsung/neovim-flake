{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.kotlin.plugins;
in
{
  options.languages.kotlin.plugins.enable = mkEnableOption "plugins for kotlin";

  config = mkIf cfg.enable {
    plugins.lsp.servers.kotlin_language_server.enable = true;
  };
}
