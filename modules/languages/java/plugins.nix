{ config, helpers, lib, pkgs, ... }:

let
  inherit (helpers) mkRaw;
  inherit (lib) mkDefault mkEnableOption mkIf optionalString;
  cfg = config.languages.java.plugins;
in
{
  options.languages.java.plugins.enable = mkEnableOption "plugins for js/ts";

  config = mkIf cfg.enable {
    plugins.dap.configurations.java = [
      {
        type = "java";
        request = "attach";
        name = "Debug (Attach) - Remote localhost:5005";
        hostName = "127.0.0.1";
        port = 5005;
      }
      {
        type = "java";
        request = "attach";
        name = "Debug (Attach) - Remote localhost:5006";
        hostName = "127.0.0.1";
        port = 5006;
      }
      {
        type = "java";
        request = "attach";
        name = "Debug (Attach) - Remote localhost:5007";
        hostName = "127.0.0.1";
        port = 5007;
      }
    ];

    plugins.nvim-jdtls = {
      enable = mkDefault config.plugins.lsp.enable;

      cmd = [ ]; # cmd will be override in extraOptions because mkRaw is not of type string
      extraOptions = {
        cmd = [
          "${pkgs.jdt-language-server}/bin/jdt-language-server"
          "-data"
          (mkRaw ''
            (os.getenv('XDG_DATA_HOME')
            .. '/nvim/jdtls/'
            .. vim.fn.fnamemodify(vim.fn.getcwd(), ':p:h:t'))
          '')
        ];

        on_attach = mkRaw ''
          function(client, bufnr)
            require'jdtls.setup'.add_commands()
            client.server_capabilities.semanticTokensProvider = nil
            ${optionalString config.plugins.dap.enable "require'jdtls'.setup_dap()"}
          end
        '';

        flags.allow_incremental_sync = true;
        root_dir = mkRaw "require('jdtls.setup').find_root({'gradlew', 'mvnw', '.git'})";
        init_options.bundles = [
          (mkRaw ''
            vim.fn.glob(
              "${pkgs.vscode-extensions.vscjava.vscode-java-debug}/share/vscode/extensions/vscjava.vscode-java-debug/server/com.microsoft.java.debug.plugin-*.jar")
          '')
        ];

        settings.java.autobuild.enabled = false;

        settings.java.imports.exclusions = [
          "**/node_modules/**"
          "**/.direnv/**"
        ];

        settings.java.configuration.runtimes = [
          {
            name = "JavaSE-11";
            path = "${pkgs.openjdk11}/lib/openjdk";
          }
          {
            name = "JavaSE-1.8";
            path = "${pkgs.openjdk8}/lib/openjdk";
          }
        ];
      };
    };
  };
}
