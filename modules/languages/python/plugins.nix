{ config, lib, ... }:

let
  inherit (lib) mkDefault mkEnableOption mkIf;
  cfg = config.languages.python.plugins;
in
{
  options.languages.python.plugins.enable = mkEnableOption "plugins for python";

  config = mkIf cfg.enable {
    plugins.lsp.servers.pyright.enable = true;
    plugins.none-ls = {
      enable = mkDefault config.plugins.lsp.enable;
      sources.formatting.black.enable = true;
    };
  };
}
