{
  languages.bo.python = {
    tabstop = 4;
    shiftwidth = 4;
    softtabstop = 4;
  };

  imports = [
    ./plugins.nix
  ];
}
