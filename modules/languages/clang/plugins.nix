{ config, lib, options, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.clang.plugins;
in
{
  options.languages.clang.plugins.enable = mkEnableOption "plugins for clang";

  config = mkIf cfg.enable {
    plugins.lsp.servers.ccls.enable = true;
  };
}
