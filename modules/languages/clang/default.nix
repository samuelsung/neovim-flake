{
  languages.bo.c = {
    tabstop = 4;
    shiftwidth = 4;
    softtabstop = 4;
  };

  languages.bo.cpp = {
    tabstop = 4;
    shiftwidth = 4;
    softtabstop = 4;
  };

  globals."c_syntax_for_h" = 1;

  imports = [
    ./plugins.nix
  ];
}
