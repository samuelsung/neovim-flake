{
  languages.bo.ocaml = {
    tabstop = 2;
    shiftwidth = 2;
    softtabstop = 2;
  };

  languages.wo.ocaml = {
    colorcolumn = "100";
  };

  imports = [
    ./plugins.nix
  ];
}
