{ config, lib, pkgs, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.ocaml.plugins;
in
{
  options.languages.ocaml.plugins.enable = mkEnableOption "plugins for ocaml";

  config = mkIf cfg.enable {
    plugins.lsp.servers.ocamllsp.enable = true;
    plugins.lsp.servers.ocamllsp.package = pkgs.ocamlPackages.ocaml-lsp;
  };
}
