{
  languages.bo.json = {
    tabstop = 2;
    shiftwidth = 2;
    softtabstop = 2;
  };

  imports = [
    ./plugins.nix
  ];
}
