{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.json.plugins;
in
{
  options.languages.json.plugins.enable = mkEnableOption "plugins for json";

  config = mkIf cfg.enable {
    plugins.lsp.servers.jsonls.enable = true;
  };
}
