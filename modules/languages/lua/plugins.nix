{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.lua.plugins;
in
{
  options.languages.lua.plugins.enable = mkEnableOption "plugins for lua";

  config = mkIf cfg.enable {
    plugins.lsp.servers.lua_ls.enable = true;
  };
}
