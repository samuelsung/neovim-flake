{ config, lib, helpers, ... }:

let
  inherit (helpers) toLuaObject;
  inherit (lib) mkDefault mkEnableOption mkIf;
  cfg = config.languages.shell.plugins;
in
{
  options.languages.shell.plugins.enable = mkEnableOption "plugins for shell";

  config = mkIf cfg.enable {
    plugins.none-ls = {
      enable = mkDefault config.plugins.lsp.enable;
      # sources.code_actions.shellcheck.enable = true;
      # sources.diagnostics.shellcheck.enable = true;
      sources.formatting.shfmt.enable = true;
      sources.formatting.shfmt.settings = toLuaObject {
        extra_args = [ "-ci" "-s" "-bn" "-i" "2" ];
      };
    };
  };
}
