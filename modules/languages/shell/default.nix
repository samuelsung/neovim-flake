{
  languages.bo.sh = {
    tabstop = 2;
    shiftwidth = 2;
    softtabstop = 2;
  };

  languages.bo.zsh = {
    tabstop = 2;
    shiftwidth = 2;
    softtabstop = 2;
  };

  imports = [
    ./plugins.nix
  ];
}
