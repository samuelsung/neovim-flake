{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.languages.html.plugins;
in
{
  options.languages.html.plugins.enable = mkEnableOption "plugins for html";

  config = mkIf cfg.enable {
    plugins.lsp.servers.html.enable = true;
  };
}
