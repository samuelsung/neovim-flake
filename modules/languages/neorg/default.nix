{ config, lib, packages, ... }:

let
  cfg = config.plugins.neorg;
  inherit (lib) mkBefore mkIf;

  neorgAction = { action, ... }@obj: obj // {
    action = "<cmd>Neorg keybind norg ${action}<CR>";
    silent = true;
  };
in
mkIf cfg.enable {
  plugins.neorg.package = packages.neorg;

  languages.bo.norg = {
    tabstop = 4;
    shiftwidth = 4;
    softtabstop = 4;
  };

  languages.wo.norg = {
    wrap = true;
    spell = true;
    conceallevel = 2;
  };

  # SEE(samuelsung): https://github.com/nvim-neorg/neorg/blob/main/lua/neorg/modules/core/keybinds/keybinds.lua

  languages.maps.norg.normal = {
    mu = neorgAction {
      action = "core.qol.todo_items.todo.task_undone";
      desc = "Mark as Undone";
    };

    mp = neorgAction {
      action = "core.qol.todo_items.todo.task_pending";
      desc = "Mark as Pending";
    };

    md = neorgAction {
      action = "core.qol.todo_items.todo.task_done";
      desc = "Mark as Done";
    };

    mh = neorgAction {
      action = "core.qol.todo_items.todo.task_on_hold";
      desc = "Mark as On Hold";
    };

    mc = neorgAction {
      action = "core.qol.todo_items.todo.task_cancelled";
      desc = "Mark as Cancelled";
    };

    mr = neorgAction {
      action = "core.qol.todo_items.todo.task_recurring";
      desc = "Mark as Recurring";
    };

    mi = neorgAction {
      action = "core.qol.todo_items.todo.task_important";
      desc = "Mark as Important";
    };

    ma = neorgAction {
      action = "core.qol.todo_items.todo.task_ambiguous";
      desc = "Mark as Ambigous";
    };

    mm = neorgAction {
      action = "core.qol.todo_items.todo.task_cycle";
      desc = "Cycle Task";
    };

    "<leader>lg" = {
      action = "<cmd>Neorg keybind all core.looking-glass.magnify-code-block<CR>";
      desc = "Looking Glass";
      silent = true;
    };

    "<leader>nn" = neorgAction {
      action = "core.dirman.new.note";
      desc = "Create New Note";
    };

    "<CR>" = neorgAction {
      action = "core.esupports.hop.hop-link";
      desc = "Jump to Link";
    };

    gd = neorgAction {
      action = "core.esupports.hop.hop-link";
      desc = "Jump to Link";
    };

    gf = neorgAction {
      action = "core.esupports.hop.hop-link";
      desc = "Jump to Link";
    };

    gF = neorgAction {
      action = "core.esupports.hop.hop-link";
      desc = "Jump to Link";
    };

    ">." = neorgAction {
      action = "core.promo.promote";
      desc = "Promote Object (Non-Recursively)";
    };

    "<." = neorgAction {
      action = "core.promo.demote";
      desc = "Demote Object (Non-Recursively)";
    };

    ">>" = neorgAction {
      action = "core.promo.promote nested";
      desc = "Promote Object (Recursively)";
    };

    "<<" = neorgAction {
      action = "core.promo.demote nested";
      desc = "Demote Object (Recursively)";
    };

    "<leader>lt" = neorgAction {
      action = "core.pivot.toggle-list-type";
      desc = "Toggle (Un)ordered List";
    };

    "<leader>li" = neorgAction {
      action = "core.pivot.invert-list-type";
      desc = "Invert (Un)ordered List";
    };

    "<leader>tc" = {
      action = "<cmd>Neorg toggle-concealer<CR>";
      desc = "Toggle Neorg Concealer";
      silent = true;
    };

    "gO" = {
      action = "<cmd>Neorg toc split<CR>";
      desc = "Open a Table of Contents";
    };
  };

  languages.maps.norg.insert = {
    "<C-t>" = neorgAction {
      action = "core.promo.promote";
      desc = "Promote Object (Recursively)";
    };

    "<C-d>" = neorgAction {
      action = "core.promo.demote";
      desc = "Demote Object (Recursively)";
    };

    "<M-CR>" = neorgAction {
      action = "core.itero.next-iteration";
      desc = "Continue Object";
    };
  };

  plugins.cmp.settings.sources = mkBefore [
    { name = "neorg"; }
  ];

  plugins.neorg.settings.load = {
    "core.completion".config.engine =
      if config.plugins.cmp.enable
      then "nvim-cmp"
      else null;
    "core.concealer".config.icon_preset = "basic";
    "core.defaults" = { __empty = null; };
    "core.dirman" = {
      config = {
        workspaces = {
          notes = "~/repos/samuelsung/notes";
        };
        default_workspace = "notes";
      };
    };
    "core.export" = { __empty = null; };
    "core.export.markdown".config.extensions = "all";
    "core.keybinds".config.default_keybinds = false;
    "core.looking-glass" = { __empty = null; };
  };
}
