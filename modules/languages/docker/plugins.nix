{ config, lib, options, ... }:

let
  inherit (lib) mkDefault mkEnableOption mkIf;
  cfg = config.languages.docker.plugins;
in
{
  options.languages.docker.plugins.enable = mkEnableOption "plugins for docker";

  config = mkIf cfg.enable {
    plugins.lsp.servers.dockerls.enable = true;
    plugins.lsp.servers.docker_compose_language_service.enable = true;

    plugins.none-ls.enable = mkDefault true;
    plugins.none-ls.sources.diagnostics.hadolint.enable = true;
  };
}
