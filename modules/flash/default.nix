{ config, lib, ... }:

let
  inherit (lib) mkIf;
  cfg = config.plugins.flash;
in
mkIf cfg.enable {
  plugins.flash.settings = {
    label = {
      before = true; # for some reason, this will cause a blank on startup
      after = false;
    };
  };

  keymaps = [
    {
      mode = [ "n" "x" "o" ];
      key = "s";
      action = ''require("flash").jump'';
      lua = true;
      options = {
        silent = true;
        desc = "Flash";
      };
    }

    {
      mode = [ "n" "x" "o" ];
      key = "S";
      action = ''require("flash").treesitter'';
      lua = true;
      options = {
        silent = true;
        desc = "Flash Treesitter";
      };
    }

    {
      mode = [ "o" ];
      key = "r";
      action = ''require("flash").remote'';
      lua = true;
      options = {
        silent = true;
        desc = "RemoteFlash";
      };
    }

    {
      mode = [ "x" "o" ];
      key = "R";
      action = ''require("flash").treesitter_search'';
      lua = true;
      options = {
        silent = true;
        desc = "Treesitter Search";
      };
    }

    {
      mode = [ "c" ];
      key = "<c-s>";
      action = ''require("flash").toggle'';
      lua = true;
      options = {
        silent = true;
        desc = "Toggle Flash Search";
      };
    }
  ];
}
