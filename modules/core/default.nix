{ config, lib, pkgs, ... }:

with lib;
with builtins;

let
  cfg = config.vim;

  wrapLuaConfig = luaConfig: ''
    lua << EOF
    ${luaConfig}
    EOF
  '';

  bindingOptions = {
    nmap = "Defines 'Normal mode' mappings";
    imap = "Defines 'Insert and Replace mode' mappings";
    vmap = "Defines 'Visual and Select mode' mappings";
    xmap = "Defines 'Visual mode' mappings";
    smap = "Defines 'Select mode' mappings";
    cmap = "Defines 'Command-line mode' mappings";
    omap = "Defines 'Operator pending mode' mappings";
    tmap = "Defines 'Terminal mode' mappings";
    nnoremap = "Defines 'Normal mode' mappings";
    inoremap = "Defines 'Insert and Replace mode' mappings";
    vnoremap = "Defines 'Visual and Select mode' mappings";
    xnoremap = "Defines 'Visual mode' mappings";
    snoremap = "Defines 'Select mode' mappings";
    cnoremap = "Defines 'Command-line mode' mappings";
    onoremap = "Defines 'Operator pending mode' mappings";
    tnoremap = "Defines 'Terminal mode' mappings";
  };
in
{
  options.vim = {
    viAlias = mkOption {
      description = "Enable vi alias";
      type = types.bool;
      default = true;
    };

    vimAlias = mkOption {
      description = "Enable vim alias";
      type = types.bool;
      default = true;
    };

    disableExModeBinding = mkOption {
      description = "Disable the Binding of entering ex mode (Q by default)";
      default = true;
      type = types.bool;
    };

    configRC = mkOption {
      description = "vimrc contents";
      type = types.lines;
      default = "";
    };

    startLuaConfigRC = mkOption {
      description = "start of vim lua config";
      type = types.lines;
      default = "";
    };

    luaConfigRC = mkOption {
      description = "vim lua config";
      type = types.lines;
      default = "";
    };

    startPlugins = mkOption {
      description = "List of plugins to startup";
      default = [ ];
      type = with types; listOf (nullOr package);
    };

    optPlugins = mkOption {
      description = "List of plugins to optionally load";
      default = [ ];
      type = with types; listOf package;
    };

    globals = mkOption {
      default = { };
      description = "Set containing global variable values";
      type = types.attrs;
    };

  } // (mapAttrs
    (_: description: mkOption ({
      inherit description;
      default = { };
      type = with types; attrsOf (nullOr str);
    }))
    bindingOptions);

  config =
    let
      filterNonNull = mappings: filterAttrs (_: v: v != null) mappings;

      globalsScript = mapAttrsFlatten
        (name: value: "let g:${name}=${toJSON value}")
        (filterNonNull cfg.globals);

      matchCtrl = it: match "Ctrl-(.)(.*)" it;

      mapKeyBinding = it:
        let
          groups = matchCtrl it;
        in
        if groups == null then
          it
        else
          "<C-${toUpper (head groups)}>${head (tail groups)}";

      mapVimBinding = prefix: mappings:
        mapAttrsFlatten
          (name: value: "${prefix} ${mapKeyBinding name} ${value}")
          (filterNonNull mappings);

      vimBindings = flatten (mapAttrsToList (k: _: mapVimBinding k config.vim.${k}) bindingOptions);
    in
    {
      vim.nnoremap = (if cfg.disableExModeBinding then {
        Q = "<nop>";
      } else { });

      vim.configRC = ''
        ${concatStringsSep "\n" globalsScript}
        " Lua config from vim.luaConfigRC
        ${wrapLuaConfig (
          concatStringsSep "\n" [ cfg.startLuaConfigRC cfg.luaConfigRC ]
        )}
        ${concatStringsSep "\n" vimBindings}
      '';
    };
}
