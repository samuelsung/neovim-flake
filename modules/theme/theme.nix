{ pkgs, config, lib, utils, ... }:

with lib;
with builtins;
with utils;

let
  cfg = config.vim.theme;
in
{

  options.vim.theme = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable Theme";
    };

    name = mkOption {
      type = types.enum [ "onedark" "tokyonight" "nord" ];
      default = "nord";
      description = ''Name of theme to use: onedark tokyonight [nord]'';
    };

    disableBackground = mkOption {
      type = types.bool;
      default = true;
      description = "Force disable background (for transparent background)";
    };

    style = mkOption {
      type = with types; nullOr (
        if (cfg.name == "tokyonight")
        then (enum [ "day" "night" "storm" ])
        else if (cfg.name == "onedark")
        then (enum [ "dark" "darker" "cool" "deep" "warm" "warmer" ])
        else str
      );
      default = null;
      internal = cfg.name == "nord";
      description = ''Theme style: "storm", darker variant "night", and "day"'';
    };
  };

  config = mkIf cfg.enable
    (
      let
        mkVimBool = val: if val then "1" else "0";
      in
      {

        vim.configRC = ''
          " need to set style before colorscheme to apply
          ${writeIf (cfg.style != null) ''
            let g:${cfg.name}_style = "${cfg.style}"
          ''}
          colorscheme ${cfg.name}

          ${writeIf cfg.disableBackground ''
            highlight Normal ctermbg=NONE guibg=NONE
            highlight LineNr ctermbg=NONE guibg=NONE
            highlight SignColumn ctermbg=NONE guibg=NONE
            highlight VertSplit ctermbg=NONE guibg=NONE
          ''}
        '';

        vim.startPlugins = with pkgs.neovimPlugins;
          if (cfg.name == "tokyonight") then [ tokyonight ]
          else if (cfg.name == "onedark") then [ onedark ]
          else [ nord-vim ];
      }
    );
}
