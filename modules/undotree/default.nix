{ config, lib, ... }:

let
  inherit (lib) mkIf;
  inherit (config.modules.remaps) keys;
  cfg = config.plugins.undotree;
in
mkIf cfg.enable {
  keymaps = [
    {
      mode = "n";
      key = "<leader>${keys.undo}";
      action = ":UndotreeToggle<CR>";
    }
  ];
}
