{ config, lib, ... }:

let
  inherit (lib) mkIf;
  cfg = config.plugins.trouble;
in
mkIf cfg.enable {
  keymaps = [
    {
      mode = "n";
      key = "<C-x>";
      action = ":TroubleToggle<CR>";
    }

    # {
    #   mode = "n";
    #   key = "<leader>xw";
    #   action = ":TroubleToggle lsp_worskpace_diagnostics<CR>";
    # }
    #
    # {
    #   mode = "n";
    #   key = "<leader>xd";
    #   action = ":TroubleToggle lsp_document_diagnostics<CR>";
    # }
    #
    # {
    #   mode = "n";
    #   key = "<leader>xq";
    #   action = ":TroubleToggle quickfix<CR>";
    # }
    #
    # {
    #   mode = "n";
    #   key = "<leader>xl";
    #   action = ":TroubleToggle loclist<CR>";
    # }
    #
    # {
    #   mode = "n";
    #   key = "<leader>xr";
    #   action = ":TroubleToggle lsp_references<CR>";
    # }
  ];
}
