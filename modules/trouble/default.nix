{ pkgs, config, lib, ... }:
with lib;
with builtins;

let
  inherit (config.vim) scope;
  cfg = config.vim.trouble;
in
{

  options.vim.trouble = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable trouble diagnostics viewer";
    };
  };

  config = mkIf (cfg.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [ trouble ];

    vim.nnoremap = {
      "<C-X>" = "<cmd>TroubleToggle<CR>";
      # "<leader>xw" = "<cmd>TroubleToggle lsp_worskpace_diagnostics<CR>";
      # "<leader>xd" = "<cmd>TroubleToggle lsp_document_diagnostics<CR>";
      # "<leader>xq" = "<cmd>TroubleToggle quickfix<CR>";
      # "<leader>xl" = "<cmd>TroubleToggle loclist<CR>";
      # "<leader>xr" = "<cmd>TroubleToggle lsp_references<CR>";
    };

    vim.luaConfigRC = ''
      -- Enable trouble diagnostics viewer
      require("trouble").setup {}
    '';
  };
}
