{ pkgs, config, lib, utils, ... }:

with lib;
with builtins;
with utils;

let
  inherit (config.vim) scope;
  cfg = config.vim.telescope;
in
{
  options.vim.telescope = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable telescope";
    };
  };

  config = mkIf (cfg.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [
      telescope
    ];

    vim.startLuaConfigRC = ''
      ${writeIf config.vim.trouble.enable ''
        local ttrouble = require('trouble.providers.telescope')
      ''}
      local tactions = require('telescope.actions');
      require('telescope').setup({
        defaults = {
          vimgrep_arguments = {
            'rg',
            '--color=never',
            '--no-heading',
            '--with-filename',
            '--line-number',
            '--column',
            '--smart-case'
          },
          prompt_prefix = "> ",
          selection_caret = "> ",
          entry_prefix = "  ",
          initial_mode = "insert",
          selection_strategy = "reset",
          sorting_strategy = "descending",
          layout_strategy = "horizontal",
          layout_config = {
            horizontal = {
              mirror = false,
              width_padding = 0.05,
              height_padding = 0.05,
              preview_width = function(_, cols)
                if cols < 70 then
                  return 0
                else
                  return math.floor(cols * 0.65)
                end
              end,
            },
            vertical = {
              mirror = false,
            },
            prompt_position = "bottom",
            preview_cutoff = 70,
          },

          mappings = {
            i = {
              --['<ESC>'] = tactions.close,
              ['<C-P>'] = false,
              ['<C-N>'] = tactions.move_selection_next,
              ['<C-E>'] = tactions.move_selection_previous,
              ${writeIf config.vim.trouble.enable ''
                ['<C-T>'] = ttrouble.open_with_trouble,
              ''}
            },
            n = {
              ['<C-P>'] = false,
              ['<C-N>'] = tactions.move_selection_next,
              ['<C-E>'] = tactions.move_selection_previous,
              ${writeIf config.vim.trouble.enable ''
                ['<C-T>'] = ttrouble.open_with_trouble,
              ''}
            },
          },
        },
      });
    '';

    vim.nnoremap = {
      "<C-P>" = "<cmd> Telescope find_files<CR>";
      "<C-S>" = "<cmd> Telescope live_grep<CR>"; # Search
      "<C-M>" = "<cmd> Telescope grep_string<CR>"; # Mentioned
      "<C-B>" = "<cmd> Telescope buffers<CR>";
      "<C-G>la" = "<cmd> Telescope git_commits<CR>";
      "<C-G>lt" = "<cmd> Telescope git_bcommits<CR>";
      "<C-G>b" = "<cmd> Telescope git_branches<CR>";
      "<C-G>g" = "<cmd> Telescope git_status<CR>";
      "<C-G>s" = "<cmd> Telescope git_stash<CR>";
      "<C-T>" = "<cmd> Telescope<CR>";
      "<C-H>" = "<cmd> Telescope help_tags<CR>";
      "<C-O>" = "<cmd> Telescope oldfiles<CR>";
    } // (if config.vim.lsp.enable then {
      "<C-L>s" = "<cmd> Telescope lsp_document_symbols<CR>";
      "<C-L>ws" = "<cmd> Telescope lsp_workspace_symbols<CR>";
      "<C-L>r" = "<cmd> Telescope lsp_references<CR>";
      "<C-L>c" = "<cmd> Telescope lsp_code_actions<CR>";
      "<C-L>t" = "<cmd> Telescope lsp_type_definitions<CR>";
      "<C-L>de" = "<cmd> Telescope lsp_definitions<CR>";
      "<C-L>di" = "<cmd> Telescope diagnostics bufnr=0<CR>";
      "<C-L>wdi" = "<cmd> Telescope diagnostics<CR>";
    } else { }) // (if config.vim.treesitter.enable then {
      "<C-N>" = "<cmd> Telescope treesitter<CR>"; # Nodes
    } else { });
  };
}
