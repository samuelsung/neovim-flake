{ config, lib, helpers, pkgs, ... }:

let
  inherit (lib) mkAfter mkIf optionals optionalAttrs;
  inherit (helpers) mkRaw;
  cfg = config.plugins.telescope;

  flash = mkRaw ''
    function(prompt_bufnr)
      require("flash").jump({
        pattern = "^",
        label = { after = { 0, 0 } },
        search = {
          mode = "search",
          exclude = {
            function(win)
              return vim.bo[vim.api.nvim_win_get_buf(win)].filetype ~= "TelescopeResults"
            end,
          },
        },
        action = function(match)
          local picker = require("telescope.actions.state").get_current_picker(prompt_bufnr)
          picker:set_selection(match.pos[1] - 1)
        end,
      })
    end
  '';
in
mkIf cfg.enable {
  keymaps = mkAfter ([
    {
      mode = "n";
      key = "<leader>pf";
      action = mkRaw "require('telescope.builtin').find_files";
      options = {
        silent = true;
        noremap = true;
        desc = "find files";
      };
    }

    # search
    {
      mode = "n";
      key = "<leader>ps";
      action = mkRaw "require('telescope.builtin').live_grep";
      options = {
        silent = true;
        noremap = true;
        desc = "global search";
      };
    }

    # cited
    {
      mode = [ "n" "v" ];
      key = "<leader>pc";
      action = mkRaw "require('telescope.builtin').grep_string";
      options = {
        silent = true;
        noremap = true;
        desc = "global search for cited word";
      };
    }

    {
      mode = "n";
      key = "<leader>b";
      action = mkRaw "require('telescope.builtin').buffers";
      options = {
        silent = true;
        noremap = true;
        desc = "browse buffers";
      };
    }

    {
      mode = "n";
      key = "<leader>h";
      action = mkRaw "require('telescope.builtin').help_tags";
      options = {
        silent = true;
        noremap = true;
        desc = "helps";
      };
    }

    {
      mode = "n";
      key = "<leader>j";
      action = mkRaw "require('telescope.builtin').jumplist";
      options = {
        silent = true;
        noremap = true;
        desc = "jumplist";
      };
    }

    {
      mode = "n";
      key = "<leader>bo";
      action = mkRaw "require('telescope.builtin').oldfiles";
      options = {
        silent = true;
        noremap = true;
        desc = "browse oldfiles";
      };
    }

    # telescope itself
    {
      mode = "n";
      key = "<leader>tb";
      action = mkRaw "require('telescope.builtin').builtin";
      options = {
        silent = true;
        noremap = true;
        desc = "telescope builtins";
      };
    }
  ] ++ (optionals config.plugins.treesitter.enable [
    {
      mode = "n";
      key = "<leader>ts";
      action = mkRaw "require('telescope.builtin').treesitter"; # symbols
      options = {
        silent = true;
        noremap = true;
        desc = "treesitter symbols";
      };
    }
  ]) ++ (optionals config.plugins.lsp.enable [
    {
      mode = "n";
      key = "ge";
      action = mkRaw "function() require('telescope.builtin').diagnostics({ bufnr = 0 }) end";
      options = {
        silent = true;
        desc = "Get diagnostics of the current buffer";
      };
    }

    {
      mode = "n";
      key = "gE";
      action = mkRaw "require('telescope.builtin').diagnostics";
      options = {
        silent = true;
        desc = "Get diagnostics of the workspace";
      };
    }

    {
      mode = "n";
      key = "gd";
      action = mkRaw "require('telescope.builtin').lsp_definitions";
      options = {
        silent = true;
        desc = "Go to lsp definition";
      };
    }

    {
      mode = "n";
      key = "glr";
      action = mkRaw "require('telescope.builtin').lsp_references";
      options = {
        silent = true;
        desc = "Go to lsp references";
      };
    }

    {
      mode = "n";
      key = "glt";
      action = mkRaw "require('telescope.builtin').lsp_type_definitions";
      options = {
        silent = true;
        desc = "Go to lsp type definition";
      };
    }

    {
      mode = "n";
      key = "gli";
      action = mkRaw "require('telescope.builtin').lsp_implementations";
      options = {
        silent = true;
        desc = "Go to lsp implementation";
      };
    }
  ]) ++ (optionals config.modules.git.enable [
    # git
    # find git files
    {
      mode = "n";
      key = "<leader>gf";
      action = mkRaw "require('telescope.builtin').git_files";
      options = {
        silent = true;
        desc = "Git files";
      };
    }

    # _git_ _projects_ _commits_
    {
      mode = "n";
      key = "<leader>gpc";
      action = mkRaw ''
        function()
          require('telescope.builtin').git_commits({
            git_command = {"git","log","--pretty=oneline","--abbrev-commit","--no-show-signature"}
          })
        end
      '';
      options = {
        silent = true;
        desc = "Git log of all files";
      };
    }

    # _git_ _commits_
    {
      mode = "n";
      key = "<leader>gc";
      action = mkRaw ''
        function()
          require('telescope.builtin').git_bcommits({
            git_command = {"git","log","--pretty=oneline","--abbrev-commit","--no-show-signature"}
          })
        end
      '';
      options = {
        silent = true;
        desc = "Git log of the current file";
      };
    }

    # git project branches
    {
      mode = "n";
      key = "<leader>gpb";
      action = mkRaw "require('telescope.builtin').git_branches";
      options = {
        silent = true;
        desc = "Git branches";
      };
    }

    {
      mode = "n";
      key = "<leader>gg";
      action = mkRaw "require('telescope.builtin').git_status";
      options = {
        silent = true;
        desc = "Git status";
      };
    }

    {
      mode = "n";
      key = "<leader>gs";
      action = mkRaw "require('telescope.builtin').git_stash";
      options = {
        silent = true;
        desc = "Git stash";
      };
    }
  ]));

  plugins.telescope.settings.defaults = {
    vimgrep_arguments = [
      "${pkgs.ripgrep}/bin/rg"
      "--color=never"
      "--no-heading"
      "--with-filename"
      "--line-number"
      "--column"
      "--smart-case"
    ];
    prompt_prefix = "> ";
    selection_caret = "> ";
    entry_prefix = "  ";
    initial_mode = "insert";
    selection_strategy = "reset";
    sorting_strategy = "descending";
    layout_strategy = "horizontal";
    layout_config = {
      horizontal = {
        mirror = false;
        width_padding = 0.05;
        height_padding = 0.05;
        preview_width = mkRaw ''
          function(_, cols)
            if cols < 70 then
              return 0
            else
              return math.floor(cols * 0.65)
            end
          end
        '';
      };
      vertical.mirror = false;
      prompt_position = "bottom";
      preview_cutoff = 70;
    };

    mappings = {
      i = {
        "<C-C>" = false;
        "<C-P>" = false;
      } // (optionalAttrs (config.modules.remaps.layout == "qwerty") {
        "<C-J>" = mkRaw "require('telescope.actions').move_selection_next";
        "<C-K>" = mkRaw "require('telescope.actions').move_selection_previous";
      }) // (optionalAttrs (config.modules.remaps.layout == "colemak") {
        "<C-N>" = mkRaw "require('telescope.actions').move_selection_next";
        "<C-E>" = mkRaw "require('telescope.actions').move_selection_previous";
      }) // (optionalAttrs config.plugins.trouble.enable {
        "<C-T>" = mkRaw "require('trouble.sources.telescope').open";
      }) // (optionalAttrs config.plugins.flash.enable {
        "<C-S>" = flash;
      });
      n = {
        "<ESC>" = mkRaw "require('telescope.actions').close";
        "<C-C>" = false;
        "<C-P>" = false;
      } // (optionalAttrs (config.modules.remaps.layout == "qwerty") {
        "<C-J>" = mkRaw "require('telescope.actions').move_selection_next";
        "<C-K>" = mkRaw "require('telescope.actions').move_selection_previous";
      }) // (optionalAttrs (config.modules.remaps.layout == "colemak") {
        "<C-N>" = mkRaw "require('telescope.actions').move_selection_next";
        "<C-E>" = mkRaw "require('telescope.actions').move_selection_previous";
      }) // (optionalAttrs config.plugins.trouble.enable {
        "<C-T>" = mkRaw "require('trouble.sources.telescope').open";
      }) // (optionalAttrs config.plugins.flash.enable {
        "<C-S>" = flash;
      });
    };
  };

  plugins.telescope.extensions.ui-select.enable = true;
}
