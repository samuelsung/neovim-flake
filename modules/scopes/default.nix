{ lib, ... }:

with lib;
with builtins;

{
  options.vim = {
    scope = mkOption {
      type = types.enum [ "neovim" "nvimpager" ];
      description = "scope of this config";
    };
  };
}
