{ pkgs, lib, config, utils, ... }:

with lib;
with builtins;
with utils;

let
  cfg = config.vim;
in
{
  options.vim = {
    colourTerm = mkOption {
      type = types.bool;
      default = true;
      description = "Set terminal up for 256 colours";
    };

    disableArrows = mkOption {
      type = types.bool;
      default = false;
      description = "Set to prevent arrow keys from moving cursor";
    };

    hideSearchHighlight = mkOption {
      type = types.bool;
      default = false;
      description = "Hide search highlight so it doesn't stay highlighted";
    };

    ignoreCaseSearch = mkOption {
      type = types.bool;
      default = true;
      description = "Ignore casing when searching";
    };

    scrollOffset = mkOption {
      type = types.int;
      default = 10;
      description =
        "Start scrolling this number of lines from the top or bottom of the page.";
    };

    wordWrap = mkOption {
      type = types.bool;
      default = true;
      description = "Enable word wrapping.";
    };

    syntaxHighlighting = mkOption {
      type = types.bool;
      default = true;
      description = "Enable syntax highlighting";
    };

    leaderKey = mkOption {
      type = types.nullOr types.str;
      default = ",";
      description = "Key to be mapped as leader key";
    };

    useSystemClipboard = mkOption {
      type = types.bool;
      default = true;
      description =
        "Make use of the clipboard for default yank and paste operations. Don't use * and +";
    };

    mouseSupport = mkOption {
      type = with types; listOf (enum [ "n" "v" "i" "c" ]);
      default = [ "n" "v" ];
      description =
        "Set modes for mouse support. a - all, n - normal, v - visual, i - insert, c - command";
    };

    lineNumberMode = mkOption {
      type = with types; enum [ "relative" "number" "relNumber" "none" ];
      default = "relNumber";
      description =
        "How line numbers are displayed. none, relative, number, relNumber";
    };

    preventJunkFiles = mkOption {
      type = types.bool;
      default = true;
      description = "Prevent swapfile, backupfile from being created";
    };

    tabWidth = mkOption {
      type = types.int;
      default = 4;
      description = "Set the width of tabs";
    };

    autoIndent = mkOption {
      type = types.bool;
      default = true;
      description = "Enable auto indent";
    };

    cmdHeight = mkOption {
      type = types.int;
      default = 1;
      description = "Height of the command pane";
    };

    updateTime = mkOption {
      type = types.int;
      default = 300;
      description =
        "The number of milliseconds till Cursor Hold event is fired";
    };

    showSignColumn = mkOption {
      type = types.bool;
      default = true;
      description = "Show the sign column";
    };

    bell = mkOption {
      type = types.enum [ "none" "visual" "on" ];
      default = "none";
      description = "Set how bells are handled. Options: on visual [none]";
    };

    mapTimeout = mkOption {
      type = types.int;
      default = 500;
      description =
        "Timeout in ms that neovim will wait for mapped action to complete";
    };

    splitBelow = mkOption {
      type = types.bool;
      default = true;
      description = "New splits will open below instead of on top";
    };

    splitRight = mkOption {
      type = types.bool;
      default = true;
      description = "New splits will open to the right";
    };

  };

  config = {
    vim.startPlugins = with pkgs.neovimPlugins; [ plenary-nvim ];

    vim.nmap =
      if (cfg.disableArrows) then {
        "<up>" = "<nop>";
        "<down>" = "<nop>";
        "<left>" = "<nop>";
        "<right>" = "<nop>";
      } else { };

    vim.imap =
      if (cfg.disableArrows) then {
        "<up>" = "<nop>";
        "<down>" = "<nop>";
        "<left>" = "<nop>";
        "<right>" = "<nop>";
      } else { };

    vim.nnoremap =
      if (cfg.leaderKey == " ") then { "<space>" = "<nop>"; } else { };

    vim.configRC = ''
      set encoding=utf-8
      set ffs=unix,dos,mac
      ${writeIf (cfg.mouseSupport != []) ''
        set mouse=${concatStrings cfg.mouseSupport}
      ''}
      set tabstop=${toString cfg.tabWidth}
      set shiftwidth=${toString cfg.tabWidth}
      set softtabstop=${toString cfg.tabWidth}
      set expandtab
      set cmdheight=${toString cfg.cmdHeight}
      set updatetime=${toString cfg.updateTime}
      set shortmess+=c " don't give |ins-completion-menu| messages.
      set tm=${toString cfg.mapTimeout}
      set hidden

      ${writeIf cfg.splitBelow ''
        set splitbelow
      ''}

      ${writeIf cfg.splitRight ''
        set splitright
      ''}

      ${writeIf cfg.showSignColumn ''
        set signcolumn=yes
      ''}

      ${writeIf cfg.autoIndent ''
        set autoindent
      ''}

      ${writeIf cfg.preventJunkFiles ''
        set noswapfile
        set nobackup
        set nowritebackup
      ''}

      ${writeIf (cfg.bell == "none") ''
        set noerrorbells
        set novisualbell
      ''}

      ${writeIf (cfg.bell == "on") ''
        set novisualbell
      ''}

      ${writeIf (cfg.bell == "visual") ''
        set noerrorbells
      ''}

      ${writeIf (cfg.lineNumberMode == "relative") ''
        set relativenumber
      ''}

      ${writeIf (cfg.lineNumberMode == "number") ''
        set number
      ''}

      ${writeIf (cfg.lineNumberMode == "relNumber") ''
        set number relativenumber
      ''}

      ${writeIf cfg.useSystemClipboard ''
        set clipboard+=unnamedplus
      ''}

      ${writeIf (cfg.leaderKey != null) ''
        let mapleader="${cfg.leaderKey}"
        let maplocalleader="${cfg.leaderKey}"
      ''}

      ${writeIf cfg.syntaxHighlighting ''
        syntax on
      ''}

      ${writeIf (cfg.wordWrap == false) ''
        set nowrap
      ''}

      ${writeIf cfg.hideSearchHighlight ''
        set nohlsearch
        set incsearch
      ''}

      ${writeIf cfg.ignoreCaseSearch ''
        set ignorecase
      ''}

      ${writeIf cfg.colourTerm ''
        set termguicolors
        set t_Co=256
      ''}
    '';
  };
}
