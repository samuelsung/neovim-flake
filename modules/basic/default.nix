{ lib, config, helpers, ... }:

let
  inherit (lib) mkBefore mkIf mkOption optionals types;
  inherit (helpers) mkRaw;
  cfg = config;
in
{
  options = {
    autoChecktime = mkOption {
      description = "run :checktime automatically";
      default = false;
      type = types.bool;
    };

    disableExModeBinding = mkOption {
      description = "Disable the Binding of entering ex mode (Q by default)";
      default = false;
      type = types.bool;
    };

    resizeSplitOnResize = mkOption {
      type = types.bool;
      default = false;
      description = "resize splits if window got resized";
    };

    createDirectoryIfNotExist = mkOption {
      type = types.bool;
      default = false;
      description = "create intermediate directory if not exist";
    };

    backToLastLocation = mkOption {
      type = types.bool;
      default = false;
      description = "back to the last location in the buffer";
    };
  };

  config = {
    autoCmd = [
      (mkIf cfg.autoChecktime {
        event = [ "FocusGained" "TermClose" "TermLeave" ];
        command = "checktime";
      })

      (mkIf cfg.resizeSplitOnResize {
        event = [ "VimResized" ];
        callback = mkRaw ''
          function()
            vim.cmd("tabdo wincmd =")
          end
        '';
      })

      (mkIf cfg.createDirectoryIfNotExist {
        event = [ "BufWritePre" ];
        callback = mkRaw ''
          function(event)
            if event.match:match("^%w%w+://") then
              return
            end
            local file = vim.loop.fs_realpath(event.match) or event.match
            vim.fn.mkdir(vim.fn.fnamemodify(file, ":p:h"), "p")
          end
        '';
      })

      (mkIf cfg.backToLastLocation {
        event = [ "BufReadPost" ];
        callback = mkRaw ''
          function()
            local exclude = { "gitcommit" }
            local buf = vim.api.nvim_get_current_buf()
            if vim.tbl_contains(exclude, vim.bo[buf].filetype) then
              return
            end
            local mark = vim.api.nvim_buf_get_mark(buf, '"')
            local lcount = vim.api.nvim_buf_line_count(buf)
            if mark[1] > 0 and mark[1] <= lcount then
              pcall(vim.api.nvim_win_set_cursor, 0, mark)
            end
          end
        '';
      })
    ];

    keymaps = mkBefore (optionals cfg.disableExModeBinding [
      {
        mode = "n";
        key = "Q";
        action = "<nop>";
      }
    ]);
  };
}
