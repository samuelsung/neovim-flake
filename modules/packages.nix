{ lib, ... }:

let
  inherit (lib) mkOption types;
  inherit (types) attrsOf package;
in
{
  options.packages = mkOption {
    default = { };
    type = attrsOf package;
    description = "package to be exposed";
  };
}
