{ pkgs, config, lib, ... }:
with lib;
with builtins;

let
  inherit (config.vim) scope;
  cfg = config.vim.keys;
in
{
  options.vim.keys = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable key binding plugins";
    };

    whichKey.enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable which-key";
    };
  };

  config = mkIf (cfg.enable && cfg.whichKey.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [
      which-key
    ];

    vim.startLuaConfigRC = ''
      -- Set variable so other plugins can register mappings
      local wk = require("which-key")
    '';

    vim.luaConfigRC = ''
      -- Set up which-key
      require("which-key").setup {}
    '';
  };
}
