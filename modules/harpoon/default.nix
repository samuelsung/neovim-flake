{ config, lib, helpers, ... }:

let
  inherit (lib) imap1 mkIf optionals;
  inherit (helpers) mkRaw;
  cfg = config.plugins.harpoon;

  mapNavFile = i: key: {
    inherit key;
    action = mkRaw "function() require('harpoon.ui').nav_file(${toString i}) end";
    options = {
      desc = "Navigate to harpoon file ${toString i}";
    };
  };
in
{
  keymaps = mkIf cfg.enable ([
    {
      mode = "n";
      key = "<leader>a";
      action = mkRaw "require('harpoon.mark').add_file";
      options = {
        desc = "Add file mark to harpoon";
      };
    }

    {
      mode = "n";
      key = "<leader>m";
      action = mkRaw "require('harpoon.ui').toggle_quick_menu";
      options = {
        desc = "Toggle harpoon menu";
      };
    }
  ] ++ (imap1 mapNavFile
    ((optionals (config.modules.remaps.layout == "colemak") [
      "<leader>n"
      "<leader>e"
      "<leader>i"
      "<leader>o"
    ]) ++ (optionals (config.modules.remaps.layout == "qwerty") [
      "<leader>j"
      "<leader>k"
      "<leader>l"
      "<leader>;"
    ]))
  ));
}
