{ config, lib, ... }:

with lib;
with builtins;
let
  cfg = config.vim.keyboardLayout;
in
{
  options.vim.keyboardLayout = {
    layout = mkOption {
      type = types.enum [ "colemak" "qwerty" ];
      default = "colemak";
      description = "Current Keyboard layout. Options: [qwerty] colemak";
    };
  };

  config =
    let
      motion =
        (if (cfg.layout == "colemak") then {
          n = "j";
          N = "J";
          e = "k";
          E = "K";
          i = "l";
          I = "L";

          "<c-w>n" = "<c-w>j";
          "<c-w>e" = "<c-w>k";
          "<c-w>i" = "<c-w>l";

          "<c-w><c-i>" = "gT";
          "<c-w><c-h>" = "gt";
        } else {
          "<c-w><c-l>" = "gT";
          "<c-w><c-h>" = "gt";
        }) // {
          "<c-w>t" = "<cmd> tab split <CR>";

          "<c-w>1" = "<cmd>tabn 1<CR>";
          "<c-w>2" = "<cmd>tabn 2<CR>";
          "<c-w>3" = "<cmd>tabn 3<CR>";
          "<c-w>4" = "<cmd>tabn 4<CR>";
          "<c-w>5" = "<cmd>tabn 5<CR>";
          "<c-w>6" = "<cmd>tabn 6<CR>";
          "<c-w>7" = "<cmd>tabn 7<CR>";
          "<c-w>8" = "<cmd>tabn 8<CR>";
          "<c-w>9" = "<cmd>tabn 9<CR>";
          "<c-w>0" = "<cmd>tabn 10<CR>";
        };

      insert =
        if (cfg.layout == "colemak") then {
          u = "i";
          U = "I";
        } else { };

      undo =
        if (cfg.layout == "colemak") then {
          l = "u";
          L = "U";
        } else { };

      next =
        if (cfg.layout == "colemak") then {
          k = "n";
          K = "N";
        } else { };

      quit = {
        "ZAQ" = "<cmd> qa!<CR>";
        "ZAZ" = "<cmd> wqa!<CR>";
      };
    in
    {
      vim.nnoremap = motion // insert // undo // next // quit;
      vim.xnoremap = motion // insert // undo // next // quit;
      vim.onoremap = motion // insert // undo // next // quit;
    };
}
