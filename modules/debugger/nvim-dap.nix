{ config, lib, helpers, ... }:

let
  inherit (lib) mkDefault optionals;
  inherit (helpers) mkRaw;
in
{

  plugins.dap = {
    enable = mkDefault config.modules.debugger.enable;

    keymaps.normal = {
      # breakpoint
      "<leader>db" = {
        action = "require('dap').toggle_breakpoint";
        desc = "Toggle debugger breakpoint";
        lua = true;
        silent = true;
      };

      # continue
      "<leader>dc" = {
        action = "require('dap').continue";
        desc = "continue a debugger breakpoint";
        lua = true;
        silent = true;
      };

      # stop
      "<leader>ds" = {
        action = "require('dap').terminate";
        desc = "Stop a debugger breakpoint";
        lua = true;
        silent = true;
      };
    };
  };

  plugins.dap-ui = {
    enable = mkDefault config.modules.debugger.enable;
  };

  keymaps = optionals config.plugins.dap-ui.enable [
    {
      mode = "n";
      key = "<leader>du";
      action = mkRaw "require('dapui').toggle";
      options = {
        desc = "Toggle debugger ui";
        silent = true;
      };
    }
  ];
}
