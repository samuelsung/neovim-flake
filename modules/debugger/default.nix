{ config, lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.modules.debugger.enable = mkEnableOption "debugger support";

  config.assertions = [{
    assertion = config.modules.debugger.enable -> config.modules.lsp.enable;
    message = "modules.lsp must be enable for modules.debugger to be enabled";
  }];

  imports = [
    ./nvim-dap.nix
  ];
}
