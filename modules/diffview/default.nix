{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf;
  cfg = config.plugins.diffview;
in
mkIf cfg.enable {
  packages.ndiff = pkgs.writeShellScriptBin "ndiff" ''
    ${config.build.package}/bin/nvim -c "DiffviewOpen $@" +tabonly
  '';

  packages.nlog = pkgs.writeShellScriptBin "nlog" ''
    ${config.build.package}/bin/nvim -c "DiffviewFileHistory $@" +tabonly
  '';
}
