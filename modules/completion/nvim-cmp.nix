{ config, lib, helpers, pkgs, ... }:

let
  inherit (helpers) mkRaw toLuaObject;
  inherit (lib) concatMapStringsSep mkAfter mkDefault mkIf mkMerge mkOption types;
  cfg = config.plugins.cmp;
in
{
  options.plugins.cmp.settings = {
    tabPrevHooks = mkOption {
      type = with types; listOf (submodule {
        options.condition = mkOption {
          type = types.str;
        };
        options.action = mkOption {
          type = types.str;
        };
      });

      default = [ ];
    };

    tabNextHooks = mkOption {
      type = with types; listOf (submodule {
        options.condition = mkOption {
          type = types.str;
        };

        options.action = mkOption {
          type = types.str;
        };
      });
      default = [ ];
    };
  };

  config.extraPackages = [
    (mkIf config.plugins.cmp-rg.enable pkgs.ripgrep)
  ];

  config.plugins.cmp = {
    enable = mkDefault config.modules.completion.enable;

    settings.preselect = "Item";
    settings.mappingPresets = [ "insert" ];

    settings.sources = mkMerge [
      [
        { name = "path"; }
        { name = "calc"; }
      ]

      (mkAfter [
        { name = "buffer"; }
        { name = "rg"; }
        { name = "tmux"; }
      ])
    ];

    settings.mapping =
      let
        tabNext = ''
          cmp.mapping(
            function(fallback)
              local cmp = require'cmp'

              local has_words_before = function()
                local line, col = unpack(vim.api.nvim_win_get_cursor(0))
                return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
              end

              if cmp.visible() then
                cmp.select_next_item()
              ${concatMapStringsSep "\n"
                ({ condition, action }: ''
                  elseif ${condition} then
                    ${action}
                '')
                cfg.settings.tabNextHooks}
              elseif has_words_before() then
                cmp.complete()
              else
                fallback()
              end
            end,
            { 'i', 's' }
          )
        '';

        tabPrev = ''
          cmp.mapping(
            function (fallback)
              local cmp = require'cmp'

              local feedkey = function(key, mode)
                vim.api.nvim_feedkeys(
                  vim.api.nvim_replace_termcodes(key, true, true, true),
                  mode,
                  true
                )
              end

              if cmp.visible() then
                cmp.select_prev_item()
              ${concatMapStringsSep "\n"
                ({ condition, action }: ''
                  elseif ${condition} then
                    ${action}
                '')
                cfg.settings.tabPrevHooks}
              else
                fallback()
              end
            end,
            { 'i', 's' }
          )
        '';
      in
      {
        "<C-Space>" = "require('cmp').mapping.complete()";
        "<C-r>" = ''
          require('cmp').mapping.confirm({
            behavior = require('cmp').ConfirmBehavior.Replace,
            select = true,
          })
        '';
        "<Tab>" = tabNext;
        "<S-Tab>" = tabPrev;
        "<C-N>" = tabNext;
        "<C-E>" = tabPrev;

        "<CR>" = toLuaObject {
          i = mkRaw ''
            function(fallback)
              local cmp = require('cmp');

              if cmp.visible() and cmp.get_active_entry() then
                cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
              else
                fallback()
              end
            end
          '';
          s = mkRaw "require('cmp').mapping.confirm({ select = true })";
          c = mkRaw ''
            require('cmp').mapping.confirm({
              behavior = require('cmp').ConfirmBehavior.Replace,
              select = true,
            })
          '';
        };
      };
  };
}
