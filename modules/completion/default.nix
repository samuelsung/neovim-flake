{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.modules.completion.enable = mkEnableOption "completion";

  imports = [
    ./nvim-cmp.nix
  ];
}
