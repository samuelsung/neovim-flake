{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.modules.autopairs.enable = mkEnableOption "autopairs";

  imports = [
    ./nvim-autopairs.nix
  ];
}
