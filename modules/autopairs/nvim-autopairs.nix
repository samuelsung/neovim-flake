{ config, lib, ... }:

let
  inherit (lib) mkDefault mkIf optionalString;
  cfg = config.plugins.nvim-autopairs;
in
{
  plugins.nvim-autopairs.enable = mkDefault config.modules.autopairs.enable;

  extraConfigLua = mkIf cfg.enable ''
    ${optionalString config.plugins.cmp.enable ''
      do
        local cmp = require('cmp')
        local cmp_autopairs = require('nvim-autopairs.completion.cmp')
        cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done())
      end
    ''}
  '';
}
