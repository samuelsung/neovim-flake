{ pkgs, lib, config, utils, ... }:

with lib;
with builtins;
with utils;

let
  inherit (config.vim) scope;
  cfg = config.vim.autopairs;
in
{
  options.vim = {
    autopairs = {
      enable = mkOption {
        type = types.bool;
        default = true;
        description = "enable autopairs";
      };

      type = mkOption {
        type = types.enum [ "nvim-autopairs" ];
        default = "nvim-autopairs";
        description = "Set the autopairs type. Options: [nvim-autopairs]";
      };
    };
  };

  config = mkIf (cfg.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [
      (if (cfg.type == "nvim-autopairs") then nvim-autopairs else null)
    ];

    vim.luaConfigRC = ''
      ${writeIf (cfg.type == "nvim-autopairs") ''
        ${writeIf cfg.enable ''
          require("nvim-autopairs").setup{}
          ${writeIf (config.vim.autocomplete.type == "nvim-compe") ''
            require('nvim-autopairs.completion.compe').setup({
              map_cr = true,
              map_complete = true,
              auto_select = false,
            })
          ''}
        ''}
      ''}
    '';
  };
}
