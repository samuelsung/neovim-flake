{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.modules.commenting.enable = mkEnableOption "commenting";

  imports = [
    ./comment-nvim.nix
  ];
}
