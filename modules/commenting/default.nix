{ pkgs, lib, config, utils, ... }:

with lib;
with builtins;
with utils;

let
  inherit (config.vim) scope;
  cfg = config.vim.commenting;
in
{
  options.vim.commenting = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "enable autocomplete";
    };

    type = mkOption {
      type = types.enum [ "comment-nvim" ];
      default = "comment-nvim";
      description = "Set the commenting plugin. Options: [comment-nvim]";
    };
  };

  config = mkIf (cfg.enable && scope == "neovim") {
    vim.startPlugins = with pkgs.neovimPlugins; [
      comment-nvim
    ];

    # HACK(samuelsung): remove the default mapping
    # TODO: remove the <nop>'s when it is resolved
    # SEE: https://github.com/numToStr/Comment.nvim/issues/78
    vim.nnoremap = {
      "gco" = "<nop>";
      "gcO" = "<nop>";
      "gcA" = "<nop>";

      "<C-C>o" = "<cmd> lua require('Comment.api').gco()<CR>";
      "<C-C>O" = "<cmd> lua require('Comment.api').gcO()<CR>";
      "<C-C>A" = "<cmd> lua require('Comment.api').gcA()<CR>";
    };

    vim.luaConfigRC = ''
      require('Comment').setup {

        ---LHS of toggle mappings in NORMAL + VISUAL mode
        toggler = {
            ---line-comment keymap
            line = '<C-C><C-C>',
        },

        ---LHS of operator-pending mappings in NORMAL + VISUAL mode
        opleader = {
            ---line-comment keymap
            line = '<C-C>',
        },
        
        mappings = {
          basic = true,
          extra = true,
          extended = false,
        },
        
        ${writeIf config.vim.treesitter.comment ''
          pre_hook = function(ctx)
            local U = require 'Comment.utils'

            local location = nil
            if ctx.ctype == U.ctype.block then
              location = require('ts_context_commentstring.utils').get_cursor_location()
            elseif ctx.cmotion == U.cmotion.v or ctx.cmotion == U.cmotion.V then
              location = require('ts_context_commentstring.utils').get_visual_start_location()
            end

            return require('ts_context_commentstring.internal').calculate_commentstring {
              key = ctx.ctype == U.ctype.line and '__default' or '__multiline',
              location = location,
            }
          end,
        ''}
      }
    '';
  };
}
