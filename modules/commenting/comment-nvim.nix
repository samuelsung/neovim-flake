{ lib, config, helpers, ... }:

let
  inherit (lib) mkForce mkIf mkOption types;
  cfg = config.plugins.comment;
in
{
  options.plugins.comment = {
    custom.enable = mkOption {
      type = types.bool;
      default = config.modules.commenting.enable;
    };

    extra = {
      above = mkOption {
        type = types.str;
        description = "above keymap";
        default = "gcO";
      };

      below = mkOption {
        type = types.str;
        description = "above keymap";
        default = "gcO";
      };

      eol = mkOption {
        type = types.str;
        description = "eol keymap";
        default = "gcA";
      };
    };
  };

  config = mkIf cfg.custom.enable {
    plugins.comment = {
      enable = mkForce false;
      settings.padding = true;
      settings.toggler.line = "<leader>cc";
      settings.toggler.block = "<leader>bc";
      settings.opleader.line = "<leader>c";
      settings.opleader.block = "<leader>b";

      settings.mappings = {
        basic = true;
        extra = true;
      };

      settings.extra = {
        above = "<leader>cO";
        below = "<leader>co";
        eol = "<leader>cA";
      };
    };

    extraPlugins = [ cfg.package ];
    extraConfigLua =
      let
        setupOptions = {
          inherit (cfg.settings)
            padding sticky ignore toggler opleader mappings extra;
        };
      in
      ''require("Comment").setup${helpers.toLuaObject setupOptions}'';
  };
}
