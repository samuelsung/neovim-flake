let
  checkNeovim = cmd: "${cmd} -es --cmd 'redir >> /dev/stdout' -c 'quit'";
in
{
  perSystem = { pkgs, self', ... }:
    {
      checks.nvim = pkgs.writeShellScript "nvim"
        (checkNeovim "${self'.packages.nvim}/bin/nvim");
      checks.nvim-all = pkgs.writeShellScript "nvim-all"
        (checkNeovim "${self'.packages.nvim-all}/bin/nvim");
      checks.nvim-page = pkgs.writeShellScript "nvim-page"
        (checkNeovim "${self'.packages.nvim-page}/bin/nvim");
    };
}
