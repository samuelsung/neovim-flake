{ self, nixpkgs, flake-utils, ... }@inputs:
# Create a nixpkg for each system
with flake-utils.lib;
(eachSystem
  [
    system.aarch64-linux
    system.x86_64-linux
  ]
  (system:
    let
      # Plugin must be same as input name
      plugins = [
        "plenary-nvim"
        "nvim-lspconfig"
        "lspsaga"
        "lspkind"
        "lsp-signature"
        "nvim-tree-lua"
        "nvim-bufferline-lua"
        "lualine"
        "nvim-compe"
        "nvim-autopairs"
        "nvim-ts-autotag"
        "nvim-web-devicons"
        "tokyonight"
        "bufdelete-nvim"
        "nvim-cmp"
        "cmp-nvim-lsp"
        "cmp-buffer"
        "cmp-vsnip"
        "cmp-path"
        "crates-nvim"
        "vim-vsnip"
        "nvim-code-action-menu"
        "trouble"
        "null-ls"
        "which-key"
        "indent-blankline"
        "nvim-cursorline"
        "sqls-nvim"
        "glow-nvim"
        "telescope"
        "rust-tools"
        "onedark"
        "nord-vim"
        "vifm-vim"
        "gitsigns-nvim"
        "nvim-ts-context-commentstring"
        "comment-nvim"
        "nvim-jdtls"
      ];

      pluginOverlay = lib.buildPluginOverlay;

      pkgs = import nixpkgs {
        inherit system;
        config = { allowUnfree = true; };
        overlays = [
          pluginOverlay
          # ISSUE(samuelsung): there is some issue with flake-utils versioning
          # got error error: attribute 'aarch64-darwin' missing
          # TODO(samuelsung): may be remove it as I don't really need master branch rnix
          # (final: prev: {
          #   rnix-lsp = inputs.rnix-lsp.defaultPackage.${system};
          # })
        ];
      };

      lib = import ./lib { inherit pkgs inputs plugins; };

      neovimBuilder = lib.neovimBuilder;
    in
    rec {
      inherit neovimBuilder;

      apps = {
        nvim = {
          type = "app";
          program = "${packages.neovim}/bin/nvim";
        };

        nvimpager = {
          type = "app";
          program = "${packages.nvimpager}/bin/nvimpager";
        };
      };

      defaultApp = packages.neovim;

      packages = neovimBuilder
        {
          config.vim = {
            lsp.enableAllLanguage = true;
          };
        };
    }
  ) // {
  overlays.default = final: prev: {
    neovim = self.packages.${prev.system}.neovim;
    nvimpager = self.packages.${prev.system}.nvimpager;
    neovimBuilder = self.packages.${prev.system}.neovimBuilder;
  };
})
