#!/bin/sh

set -eu

read -r msg

checkenv() {
  printenv "$1" >/dev/null || {
    echo "missing variable $1" >&2
    exit 1
  }
}

# ENV
checkenv title
checkenv owner
checkenv repo_name
checkenv source_branch
checkenv target_branch
checkenv gitlab_token

echo "title: ${title:?}"
echo "owner: ${owner:?}"
echo "assignee_ids: ${assignee_ids:=0}"
echo "reviewer_ids: ${reviewer_ids:=0}"
echo "repo_name: ${repo_name:?}"
echo "source_branch: ${source_branch:?}"
echo "target_branch: ${target_branch:?}"
echo "labels: ${labels:=}"
echo "remove_source_branch: ${remove_source_branch:=true}"

id=$(curl -s "https://gitlab.com/api/v4/users/${owner}/projects" | jq -r "map(select(.name == \"${repo_name}\"))[0].id")

[ -z "$id" ] || [ "$id" = "null" ] && {
  echo "repository $owner/$repo_name do not exist." >&2
  exit 1
}

echo "id: ${id}"
echo "msg: ${msg}"

existing_request=$(curl -s "https://gitlab.com/api/v4/projects/${id}/merge_requests?source_branch=${source_branch}&state=opened" | jq '.[].iid')

if [ -n "$existing_request" ]; then
  echo "existing pull request: !$existing_request"

  echo "closing !${existing_request}."

  res=$(curl -s --request PUT --header "PRIVATE-TOKEN: ${gitlab_token:?}" "https://gitlab.com/api/v4/projects/$id/merge_requests/${existing_request}?state_event=close")

  echo "closed !${existing_request}. state: $(echo "$res" | jq '.state')"

fi

data="{
  \"source_branch\": \"${source_branch}\",
  \"target_branch\": \"${target_branch}\",
  \"title\": \"${title}\",
  \"assignee_ids\": \"${assignee_ids}\",
  \"reviewer_ids\": \"${reviewer_ids}\",
  \"labels\": \"${labels}\",
  \"remove_source_branch\": ${remove_source_branch},
  \"description\": ${msg}
}"

echo "create new merge request."

res=$(curl -s --request POST --header "PRIVATE-TOKEN: ${gitlab_token}" "https://gitlab.com/api/v4/projects/$id/merge_requests" --header "Content-Type: application/json" --data "${data}")

new_request=$(echo "$res" | jq '.iid')

echo "created new merge request (!${new_request})."

if [ -n "$new_request" ] && [ -n "$existing_request" ]; then
  echo "updating existing merge request note (!$existing_request)"

  res=$(curl -s --request POST --header "PRIVATE-TOKEN: ${gitlab_token}" "https://gitlab.com/api/v4/projects/$id/merge_requests/${existing_request}/notes?body=Closed.+In+favor+of+!${new_request}")

  echo "updated existing merge request note (!$existing_request): $(echo "$res" | jq '.body')"
fi
