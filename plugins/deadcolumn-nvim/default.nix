{ config, lib, helpers, packages, ... }:
let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.plugins.deadcolumn-nvim;
in
{
  options.plugins.deadcolumn-nvim =
    helpers.plugins.neovim.extraOptionsOptions
    // {
      enable = mkEnableOption "deadcolumn.nvim";

      package = lib.mkPackageOption packages "deadcolumn-nvim" {
        nullable = false;
      };
    };

  config = mkIf cfg.enable {
    extraConfigLua =
      let
        options = cfg.extraOptions;
      in
      ''
        require('deadcolumn').setup(${helpers.toLuaObject options})
      '';

    extraPlugins = [
      cfg.package
    ];
  };
}
