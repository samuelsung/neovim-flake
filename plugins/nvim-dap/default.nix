{ config, lib, ... }:

let
  inherit (lib) mapAttrs mkIf mkOption types;
  cfg = config.plugins.dap;
in
{
  options.plugins.dap = {
    keymaps = mkOption {
      type = types.attrs;
      description = "Keymaps for nvim-dap.";
      default = { };
      example = {
        normal."<leader>du".action = "require('dap').continue";
        normal."<leader>db" = "require('dap').toggle_breakpoint";
      };
    };
  };

  config = mkIf cfg.enable {
    languages.maps = mapAttrs
      (_: _: cfg.keymaps)
      cfg.configurations;
  };
}
