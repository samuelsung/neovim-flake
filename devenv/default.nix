{ inputs, lib, ... }:

let
  inherit (lib) mkForce;
in
{
  imports = [ inputs.devenv.flakeModule ];
  perSystem = ctx@{ system, pkgs, ... }: {
    checks = {
      pre-commit = ctx.config.devenv.shells.default.pre-commit.run;
    };

    devenv.shells.default = {
      pre-commit = {
        src = ../.;
        hooks = {
          deadnix.enable = true;
          nixpkgs-fmt.enable = true;
          statix.enable = true;
          statix.settings.ignore = [
            "srcs\\.nix"
          ];
          shfmt.enable = true;
          shfmt.entry = mkForce "${pkgs.shfmt}/bin/shfmt -ci -s -bn -i 2 -l -w";
          shfmt.excludes = [
            "p10k\\.zsh"
            "secret\\.sh"
          ];

          commitlint = inputs.commitlint-config.hook.${system};
        };
      };
    };
  };
}
