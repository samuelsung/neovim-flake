{ inputs, lib, ... }:

let
  inherit (lib) listToAttrs replaceStrings;
  replacePeriod = replaceStrings [ "." ] [ "-" ];
in
{
  perSystem = { system, pkgs, srcs, ... }:
    let
      inherit (pkgs.vimUtils) buildVimPlugin;

      mapPluginSrcs = xs: listToAttrs (map
        (src: {
          name = replacePeriod src.name;
          value = buildVimPlugin {
            inherit (src) src version;
            pname = src.name;
          };
        })
        xs);

      neorgPkgs = import inputs.nixpkgs {
        inherit system;
        overlays = [
          inputs.neorg-overlay.overlays.default
        ];
        config = { };
      };
    in
    {
      packages = mapPluginSrcs
        (with srcs.github-repo; [
          Bekaboo."deadcolumn.nvim"
          lmburns."lf.nvim"
        ]) // {
        inherit (neorgPkgs.vimPlugins) neorg;
      };
    };
}
