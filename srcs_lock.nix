{ fetchFromGitHub
}:

{
  github-repo."Bekaboo"."deadcolumn.nvim" = {
    name = "deadcolumn.nvim";
    publisher = "Bekaboo";
    version = "897c905aef1a268ce4cc507d5cce048ed808fa7a";
    npmDepsHash = "";
    src = fetchFromGitHub {
      owner = "Bekaboo";
      repo = "deadcolumn.nvim";
      rev = "897c905aef1a268ce4cc507d5cce048ed808fa7a";
      sha256 = "1xwiiy4fpinxvya2vykfkkrh1pbich6kp9d9pw2lv1jxz6wh5gki";
    };
  };
  github-repo."lmburns"."lf.nvim" = {
    name = "lf.nvim";
    publisher = "lmburns";
    version = "69ab1efcffee6928bf68ac9bd0c016464d9b2c8b";
    npmDepsHash = "";
    src = fetchFromGitHub {
      owner = "lmburns";
      repo = "lf.nvim";
      rev = "69ab1efcffee6928bf68ac9bd0c016464d9b2c8b";
      sha256 = "0hwd90ahqm3hh78qhz7id064bkx8xrinfn133d74ws30gf0y9kfa";
    };
  };
}
