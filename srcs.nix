{ inputs, ... }:

{
  imports = [ inputs.misc.flakeModules.srcs ];
  srcs = {
    root = ./.;
    remotes = {
      vscode-extensions = [ ];

      github-release = [ ];

      github-repo = [
        {
          publisher = "Bekaboo";
          name = "deadcolumn.nvim";
          ref = "master";
        }
      ];
    };
  };
}
