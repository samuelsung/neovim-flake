# My personal Neovim/Nvimpager configuration

# Origin
This flake is ~~shamelessly copied from~~ based on [jordanisaacs's neovim-flake](https://github.com/jordanisaacs/neovim-flake) (Thank you 🤓)

# Apps
```
nix run .#
```
This will start wrapped neovim.

# References
- [jordanisaacs's neovim-flake](https://github.com/jordanisaacs/neovim-flake)
- [nanotee's nvim-lua-guide](https://github.com/nanotee/nvim-lua-guide)
