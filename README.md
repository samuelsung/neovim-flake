# My personal neovim/page configuration

Neovim, page configuration using [nixvim](https://github.com/pta2002/nixvim).

# Apps
```sh
# update additional packages in srce.json
nix run .#update-srcs

# run neovim with no additional config
nix run .#nvim

# run neovim with all modules enable
nix run .#nvim-all

# run neovim with default configuration using page
nix run .#nvim-page

# run page with default configuration
nix run .#nvim-page
```

# Home-manager modules
```nix
{ config, ... }:

{
  imports = [ neovim-flake.homeManagerModules.default ];

  programs.neovim-flake = {
    # common modules that applies to both neovim and page
    modules = [{
      # ...
    }];

    neovim.enable = true; # enable neovim
    neovim.setupEditor = true; # set $EDITOR variable
    # modules that applies to both neovim
    neovim.modules = [{
      # ...
    }];

    page.enable = true; # enable page
    page.setupPager = true; # set $PAGER variable
    # modules that applies to page's neovim
    page.modules = [{
      # ...
    }];
  };

  # neovim package
  xxxx = "${config.programs.neovim-flake.neovim.finalPackage}/bin/nvim";

  # neovim package for page
  yyyy = "${config.programs.neovim-flake.page.neovim.finalPackage}/bin/nvim";

  # page package
  zzzz = "${config.programs.neovim-flake.page.finalPackage}/bin/page";
}
```

# Credit
This flake is originally based on [jordanisaacs's neovim-flake](https://github.com/jordanisaacs/neovim-flake), then migrated to [pta2002's nixvim](https://github.com/pta2002/nixvim).

# References
- [pta2002's nixvim](https://github.com/pta2002/nixvim)
- [jordanisaacs's neovim-flake](https://github.com/jordanisaacs/neovim-flake)
- [nanotee's nvim-lua-guide](https://github.com/nanotee/nvim-lua-guide)
