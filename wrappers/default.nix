{ inputs, ... }:

{
  imports = [
    ./hm.nix
    ./standalone.nix
    ./perSystemBuilder.nix
    ./perSystemPageBuilder.nix
  ];

  perSystem = { config, lib, pkgs, ... }:
    let
      inherit (inputs.nixvim.lib.nixvim.modules) evalNixvim;
    in
    {
      builder = module:
        let
          eval = evalNixvim {
            modules = [
              module
              ../modules
              { nixpkgs = { inherit pkgs; }; wrapRc = true; }
            ];
            extraSpecialArgs = { inherit (config) packages; };
          };
        in
        eval.config.build.package.overrideAttrs (args: {
          passthru = (args.passthru or { }) // eval.config.packages;
        });

      pageBuilder = neovimPackage: pkgs.symlinkJoin rec {
        name = "page";
        paths = [ pkgs.page ];
        buildInputs = [ pkgs.makeWrapper ];
        postBuild = "wrapProgram $out/bin/${name} --prefix PATH : ${
          pkgs.lib.makeBinPath [ neovimPackage ]
        }";
      };
    };
}
