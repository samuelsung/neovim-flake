{ lib, flake-parts-lib, ... }:
let
  inherit (lib)
    mkOption
    types;
  inherit (flake-parts-lib)
    mkTransposedPerSystemModule;
in
mkTransposedPerSystemModule {
  name = "builder";
  option = mkOption {
    type = types.functionTo types.package;
  };
  file = ./perSystemBuilder.nix;
}
