{ lib, flake-parts-lib, ... }:
let
  inherit (lib)
    mkOption
    types;
  inherit (flake-parts-lib)
    mkTransposedPerSystemModule;
in
mkTransposedPerSystemModule {
  name = "pageBuilder";
  option = mkOption {
    type = types.functionTo types.package;
  };
  file = ./perSystemPageBuilder.nix;
}
