{ inputs, moduleWithSystem, ... }:

{
  flake.homeManagerModules.default = moduleWithSystem (
    # deadnix: skip
    perSystem@{ config }:
    # deadnix: skip
    home@{ config, lib, pkgs, ... }:

    let
      inherit (lib) attrValues mkEnableOption mkForce mkMerge mkOption mkIf;
      inherit (perSystem.config) packages pageBuilder;
      inherit (inputs.nixvim.lib.nixvim.modules) evalNixvim;

      cfg = config.programs.neovim-flake;
    in
    {
      options = {
        programs.neovim-flake.neovim = mkOption {
          default = { };
          inherit ((evalNixvim {
            modules = [
              ../modules
              {
                options = {
                  enable = mkEnableOption "neovim-flake's neovim";
                  defaultEditor = mkEnableOption "Set neovim-flake's neovim as the default editor";
                };
                config.page.enable = mkForce false;
                config.wrapRc = mkForce true;
                config.nixpkgs = { inherit pkgs; };
                config.plugins.yazi.yaziPackage = home.config.programs.yazi.package;
              }
            ];
            extraSpecialArgs = { inherit packages; };
          })) type;
        };

        programs.neovim-flake.vscode = mkOption {
          default = { };
          inherit ((evalNixvim {
            modules = [
              ../modules
              {
                options = {
                  enable = mkEnableOption "neovim-flake's vscode-neovim";
                };
                config.vscode.enable = mkForce true;
                config.wrapRc = mkForce true;
                config.nixpkgs = { inherit pkgs; };
                config.plugins.yazi.yaziPackage = home.config.programs.yazi.package;
              }
            ];
            extraSpecialArgs = { inherit packages; };
          })) type;
        };

        programs.neovim-flake.page = mkOption {
          default = { };
          inherit ((evalNixvim {
            modules = [
              ../modules
              {
                options = {
                  enable = mkEnableOption "neovim-flake's page";
                  defaultPager = mkEnableOption "Set neovim-flake's page as the default pager";
                };
                config.page.enable = mkForce true;
                config.wrapRc = mkForce true;
                config.nixpkgs = { inherit pkgs; };
                config.plugins.yazi.yaziPackage = home.config.programs.yazi.package;
              }
            ];
            extraSpecialArgs = { inherit packages; };
          })) type;
        };
      };

      config = mkMerge [
        (mkIf cfg.neovim.enable {
          inherit (cfg.neovim) warnings assertions;

          home.packages = [
            cfg.neovim.build.package
          ] ++ (attrValues cfg.neovim.packages);
          home.sessionVariables = mkIf cfg.neovim.defaultEditor {
            EDITOR = "${cfg.neovim.build.package}/bin/nvim";
          };
        })

        (mkIf cfg.page.enable {
          inherit (cfg.page) warnings assertions;

          home.packages = [
            (pageBuilder cfg.page.build.package)
          ];
          home.sessionVariables = mkIf cfg.page.defaultPager {
            PAGER = "${(pageBuilder cfg.page.build.package)}/bin/page";
          };
        })
      ];
    }
  );
}
