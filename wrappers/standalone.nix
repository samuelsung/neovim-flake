{
  perSystem =
    { config, ... }:

    {
      packages.nvim = config.builder { };

      packages.nvim-all = config.builder {
        modules.debugger.enable = true;
        modules.lsp.enable = true;
        modules.snippets.enable = true;

        languages.clang.plugins.enable = true;
        languages.css.plugins.enable = true;
        languages.docker.plugins.enable = true;
        languages.html.plugins.enable = true;
        languages.java.plugins.enable = true;
        languages.js.plugins.enable = true;
        languages.json.plugins.enable = true;
        languages.kotlin.plugins.enable = true;
        languages.latex.plugins.enable = true;
        languages.lua.plugins.enable = true;
        languages.markdown.plugins.enable = true;
        languages.nix.plugins.enable = true;
        languages.ocaml.plugins.enable = true;
        languages.python.plugins.enable = true;
        languages.rust.plugins.enable = true;
        languages.shell.plugins.enable = true;
        languages.spelling.plugins.enable = true;
        languages.sql.plugins.enable = true;
        languages.wgsl.plugins.enable = true;
      };

      packages.nvim-page = config.builder { page.enable = true; };

      packages.nvim-vscode = config.builder { vscode.enable = true; };

      packages.page = config.pageBuilder config.packages.nvim-page;
    };
}
