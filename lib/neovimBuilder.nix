{ pkgs, lib ? pkgs.lib, ... }:

{ config }:
let
  neovimPlugins = pkgs.neovimPlugins;

  utils = import ./utils.nix { inherit pkgs; };

  # attempt fix for libstdc++.so.6 no file or directory
  myNeovimUnwrapped = pkgs.neovim-unwrapped.overrideAttrs (prev: {
    propagatedBuildInputs = with pkgs; [ pkgs.stdenv.cc.cc.lib ripgrep ];
  });

  vimOptions = scope: lib.evalModules {
    modules = [
      { imports = [ ../modules ]; }
      config
      { vim = { inherit scope; }; }
    ];

    specialArgs = {
      inherit pkgs utils;
    };
  };

  neovimConfig = (vimOptions "neovim").config.vim;
  nvimpagerConfig = (vimOptions "nvimpager").config.vim;
in
rec {
  nvimpager-neovim = pkgs.wrapNeovim myNeovimUnwrapped {
    viAlias = false;
    vimAlias = false;
    configure = {
      customRC = nvimpagerConfig.configRC;

      packages.myVimPackage = with neovimPlugins; {
        start = builtins.filter (f: f != null) nvimpagerConfig.startPlugins;
        opt = nvimpagerConfig.optPlugins;
      };
    };
  };

  nvimpager = (pkgs.nvimpager.override {
    neovim = nvimpager-neovim;
  }).overrideAttrs ({ preBuild, checkInputs, ... }: {
    # remove nvimpager overriding the rc
    preBuild = preBuild + ''
      substituteInPlace nvimpager --replace "\${"$"}{rc:+-u \"\${"$"}rc\"}" ""
    '';

    doCheck = false; # wrapped neovim will conflict with the test case orz
  });

  neovim = pkgs.wrapNeovim myNeovimUnwrapped {
    viAlias = neovimConfig.viAlias;
    vimAlias = neovimConfig.vimAlias;
    configure = {
      customRC = neovimConfig.configRC;

      packages.myVimPackage = with neovimPlugins; {
        start = builtins.filter (f: f != null) neovimConfig.startPlugins;
        opt = neovimConfig.optPlugins;
      };
    };
  };
}
